import React, {useEffect, useRef} from 'react';
import {StatusBar} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import {NavigationContainer} from '@react-navigation/native';
import {
  CardStyleInterpolators,
  createStackNavigator,
} from '@react-navigation/stack';
const Stack = createStackNavigator();
import {GoogleSignin} from '@react-native-google-signin/google-signin';
import auth from '@react-native-firebase/auth';
import dayjs from 'dayjs';
require('dayjs/locale/ru');
dayjs.locale('ru');
import useAuthContext, {AuthProvider} from './src/commons/AuthContext';
import {CafeProvider} from './src/commons/CafeContext';
import {MenuProvider} from './src/commons/MenuContext';
import Login from './src/screens/Login';
import RegCafes from './src/screens/RegCafes';
import RegForm from './src/screens/RegForm';
import RegStatus from './src/screens/RegStatus';
import Home from './src/screens/Home';
import EditCafe from './src/screens/EditCafe';
import NewMenu from './src/screens/NewMenu';
import Staff from './src/screens/Staff';
import Image from './src/screens/Image';
import EditPost from './src/screens/EditPost';
import Kitchens from './src/screens/Kitchens';
import {enableScreens} from 'react-native-screens';
enableScreens(true);
GoogleSignin.configure({
  webClientId:
    '50061477604-pc3k234sop1mdllghevp0jj0putjarij.apps.googleusercontent.com',
});

export default function App() {
  return (
    <AuthProvider>
      <CafeProvider>
        <MenuProvider>
          <StatusBar barStyle="dark-content" backgroundColor="white" />
          <Routes />
        </MenuProvider>
      </CafeProvider>
    </AuthProvider>
  );
}

const Routes = () => {
  const mounted = useRef(true),
    {myid, getDBUser, status} = useAuthContext();

  useEffect(() => {
    auth().onAuthStateChanged(
      usr => usr && !myid && mounted.current && getDBUser(usr.email),
    );
    setTimeout(() => SplashScreen.hide(), 1250);
    return () => (mounted.current = false);
  }, []);

  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}>
        {!myid && (
          <Stack.Screen
            name="Login"
            component={Login}
            options={{animationEnabled: false}}
          />
        )}
        {myid && status != 'approved' && (
          <Stack.Screen name="Unapproved" component={Unapproved} />
        )}
        {myid && status == 'approved' && (
          <Stack.Screen name="Approved" component={Approved} />
        )}
        <Stack.Group
          screenOptions={{
            presentation: 'transparentModal',
            animationEnabled: false,
          }}>
          <Stack.Screen name="IMG" component={Image} />
          <Stack.Screen name="Kitchens" component={Kitchens} />
        </Stack.Group>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const Unapproved = () => {
  const {status} = useAuthContext();
  return (
    <Stack.Navigator
      initialRouteName={!status ? 'RegCafes' : 'RegStatus'}
      screenOptions={{
        headerShown: false,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}>
      {status != 'applied' && (
        <Stack.Screen
          name="RegCafes"
          component={RegCafes}
          options={{animationEnabled: !status ? false : true}}
        />
      )}
      <Stack.Screen name="RegForm" component={RegForm} />
      <Stack.Screen
        name="RegStatus"
        component={RegStatus}
        options={{animationEnabled: false}}
      />
    </Stack.Navigator>
  );
};

const Approved = () => (
  <Stack.Navigator
    initialRouteName="Home"
    screenOptions={{
      headerShown: false,
      cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
    }}>
    <Stack.Screen
      name="Home"
      component={Home}
      options={{animationEnabled: false}}
    />
    <Stack.Screen name="Edit" component={EditPost} />
    <Stack.Screen name="EditCafe" component={EditCafe} />
    <Stack.Screen name="NewMenu" component={NewMenu} />
    <Stack.Screen name="Staff" component={Staff} />
  </Stack.Navigator>
);
