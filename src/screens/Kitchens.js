import React, {useState, useEffect, useRef, useCallback} from 'react';
import {StatusBar, View} from 'react-native';
import styled from 'styled-components';
import fire from '@react-native-firebase/firestore';
const db = fire();
import BottomSheet, {BottomSheetFlatList} from '@gorhom/bottom-sheet';
import {paddBottom, wheight} from '../commons/models';
import usecontext from '../commons/CafeContext';
import {
  handleComponent,
  Button,
  CheckboxIcon,
  Loader,
} from '../commons/IconsForms';

export default ({navigation: {navigate, goBack: goback2}}) => {
  const goBack = () => navigate('EditCafe');
  const onAnimate = useCallback(
    (from, to) => from > 0 && to < 1 && goBack(),
    [],
  );
  const backdropComponent = () => <BackView onPress={goBack} />;

  return (
    <BottomSheet
      snapPoints={[1, wheight * 0.75]}
      index={1}
      handleHeight={0}
      {...{onAnimate, handleComponent, backdropComponent}}>
      {/* <BackView onPress={goBack}>
      <View style={{height: 100, backgroundColor: 'red'}} /> 
      </BackView> */}
      <StatusBar backgroundColor="rgba(0, 0, 0, 0.5)" />
      <BSList {...{goBack}} />
    </BottomSheet>
  );
};

const BSList = ({goBack}) => {
  const {
      cafe: {kitchens: init},
      updateFields,
    } = usecontext(),
    [chosen, setChosen] = useState(init),
    [all, setAll] = useState(),
    quant = chosen.length,
    changed = quant != init.length || chosen.some(k => !init.includes(k));

  useEffect(() => {
    const getkits = async () =>
      await db
        .collection('kitchens')
        .get()
        .then(query => {
          let set = new Set();
          query.forEach(dc => set.add(dc.data().name));
          setAll([...set].sort());
        });
    getkits();
  }, []);

  const pick = k =>
    setChosen(prv => {
      let newarr = [...prv];
      if (!prv.includes(k)) newarr.push(k);
      else {
        ind = newarr.findIndex(it => it == k);
        newarr.splice(ind, 1);
      }
      return newarr;
    });

  const reset = () => setChosen(init);
  const save = async () =>
    changed &&
    (quant > 0 ? (await updateFields({kitchens: chosen}), goBack()) : reset());

  const renderItem = ({item: k}) => {
    let active = chosen.includes(k);
    return (
      <Touch onPress={() => pick(k)} activeOpacity={0.5}>
        <FRow>
          {chosen && (
            <CheckboxIcon
              {...{active}}
              style={{marginRight: !active ? 16 : 11}}
            />
          )}
          <Text>{k}</Text>
        </FRow>
      </Touch>
    );
  };

  return (
    <>
      <TitleRow>
        <Title>Типы кухни</Title>
        <Button
          text={quant > 0 ? 'Сохранить' : 'Сбросить'}
          onPress={save}
          style={!changed && {backgroundColor: '#e7e7e7'}}
        />
      </TitleRow>
      {!all && <Loader />}
      {all && (
        <BottomSheetFlatList
          data={all}
          keyExtractor={i => i}
          {...{renderItem}}
          contentContainerStyle={{paddingBottom: 20 + paddBottom}}
        />
      )}
    </>
  );
};

const Touch = styled.TouchableOpacity``;
const Press = styled.Pressable``;
const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;

const BackView = styled(Press)`
  flex: 1;
  background-color: rgba(0, 0, 0, 0.5);
`;

const TitleRow = styled(Row)`
  justify-content: space-between;
  padding: 17px 16px 9px;
`;

const Title = styled.Text`
  font-family: 'Rubik-Medium';
  font-size: 18px;
`;

const FRow = styled(Row)`
  padding: 0 16px;
  height: 42px;
`;

const Text = styled.Text`
  font-family: 'Rubik-Regular';
  font-size: 14px;
  line-height: 18px;
  flex: 1;
  flex-shrink: 1;
`;
