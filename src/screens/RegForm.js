import React, {useState, useRef, useCallback} from 'react';
import {View, ScrollView, StatusBar} from 'react-native';
import styled from 'styled-components/native';
import PhoneInput from 'react-phone-number-input/input';
import useAContext from '../commons/AuthContext';
import {Button, Title, BackTouch, shadow2} from '../commons/IconsForms';
import {paddBottom, isIOS, paddTop} from '../commons/models';
import {LogOut} from './RegCafes';

export default ({
  route: {
    params: {cafe},
  },
  navigation: {push, goBack},
}) => {
  const {profile, myid, name, fname, applyCafe, logout} = useAContext(),
    {email, phone: initp} = profile,
    {id, name: title, net, firstApply} = cafe,
    hasAdmin = firstApply && firstApply != myid,
    [contact, setContact] = useState({name, fname, phone: initp}),
    {phone} = contact;

  const apply = async () => {
    if (!contact.name.trim()[1]) return alert('Некорректное имя');
    // console.log(contact, phone, phone?.trim()[8], !phone || !phone.trim()[8]);
    if (!hasAdmin && !phone?.trim()[8]) return alert('Некорректный телефон');
    else {
      await applyCafe({id, hasAdmin}, contact);
      push('RegStatus', {cafe});
    }
  };

  return (
    <Container behavior={isIOS ? 'padding' : null}>
      <StatusBar barStyle="dark-content" backgroundColor="#fff" />

      <NavRow>
        <BackTouch onPress={goBack} />
        <LogOut {...{logout}} />
      </NavRow>
      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
          padding: 16,
          paddingTop: 12,
          paddingBottom: 24 + paddBottom,
        }}
        keyboardShouldPersistTaps="handled">
        <Body>
          <View>
            <Header {...{cafe}} />
            <Address>
              {hasAdmin
                ? `Мы отправим ${title} заявку с вашими данными, чтобы вас подключили к системе`
                : `Вы представитель ${
                    (net ? 'сети ' : '') + title
                  }? Отправьте заявку на подключение, и мы свяжемся с вами в ближайшее время`}
            </Address>
            <Text style={{marginTop: 16}}>
              <Address>E-mail</Address> {email}
            </Text>
          </View>

          <NameInputs {...{contact, setContact, hasAdmin}} />

          <Button onPress={apply} text="Отправить заявку" big />
        </Body>
      </ScrollView>
    </Container>
  );
};

export const Header = ({cafe: {logo, name, address, net}}) => (
  <HeaderRow>
    {logo && (
      <View style={shadow2}>
        <Logo source={{uri: logo}} />
      </View>
    )}
    <View style={{flexShrink: 1}}>
      <Title>{name}</Title>
      <Address>{net ? net.length + ' заведений сети' : address}</Address>
    </View>
  </HeaderRow>
);

const NameInputs = ({contact, setContact, hasAdmin}) => {
  const [name, setName] = useState(contact.name),
    [fname, setFname] = useState(contact.fname),
    [phone, setPhone] = useState(contact.phone || '+380'), //|| '+380'),
    fnameref = useRef(),
    phoneref = useRef(),
    toFname = () => fnameref?.current.focus(),
    toPhone = () => phoneref?.current.focus();
  return (
    <View style={{marginVertical: 42}}>
      {['Имя', 'Фамилия'].map((it, i) => {
        let i0 = i == 0,
          update = () =>
            setContact(pr => (i0 ? {...pr, name} : {...pr, fname}));
        return (
          <Input
            placeholder={it}
            ref={!i0 ? fnameref : null}
            value={i0 ? name : fname}
            onChangeText={i0 ? setName : setFname}
            onEndEditing={update}
            onSubmitEditing={i0 ? toFname : !hasAdmin ? toPhone : update}
            blurOnSubmit={i0 || !hasAdmin ? false : true}
            returnKeyType="next"
            keyboardType="default"
            placeholderTextColor="#888"
            key={it}
          />
        );
      })}

      {!hasAdmin && (
        <PhoneInput
          smartCaret={false}
          placeholder="Телефон"
          ref={phoneref}
          inputComponent={PhoneTextInput}
          value={phone}
          onChange={setPhone}
          onEndEditing={() => setContact(pr => ({...pr, phone}))}
          maxLength={17}
          keyboardType="phone-pad"
          returnKeyType="next"
          placeholderTextColor="#888"
        />
      )}
    </View>
  );
};

function PhoneTextInput({onChange, ...rest}, ref) {
  const onChangeText = useCallback(
    value => {
      onChange({
        preventDefault() {
          this.defaultPrevented = true;
        },
        target: {value},
      });
    },
    [onChange],
  );
  return <Input {...{ref, onChangeText, ...rest}} />;
}
PhoneTextInput = React.forwardRef(PhoneTextInput);

const Touch = styled.TouchableOpacity``;
const Press = styled.Pressable``;

const Row = styled.View`
  flex-direction: row;
`;

const Container = styled.KeyboardAvoidingView`
  flex: 1;
  background: white;
  padding-top: ${paddTop}px;
`;

const Body = styled.View`
  flex: 1;
  justify-content: space-between;
`;

const HeaderRow = styled(Row)`
  padding-bottom: 16px;
`;

const NavRow = styled(Row)`
  justify-content: space-between;
  align-items: center;
  padding-right: 16px;
`;

const Logo = styled.Image`
  width: 32px;
  height: 32px;
  border-radius: 18px;
  margin-right: 12px;
`;

const Text = styled.Text`
  font-family: 'Rubik-Regular';
  font-size: 16px;
  line-height: 24px;
  flex-shrink: 1;
`;

const Address = styled(Text)`
  color: #888;
`;

const Input = styled.TextInput`
  font-family: 'Rubik-Regular';
  font-size: 16px;
  border-bottom-width: 1px;
  border-bottom-color: #e7e7e7;
  padding: 16px 0;
`;
