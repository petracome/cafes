import React, {useState, useEffect} from 'react';
import {View, Linking, StatusBar} from 'react-native';
import styled from 'styled-components/native';
import fire from '@react-native-firebase/firestore';
const db = fire();
import useAContext from '../commons/AuthContext';
import {
  Title,
  Button,
  smileIcon,
  sadIcon,
  Telegram,
  EmailIcon,
  Loader,
} from '../commons/IconsForms';
import {paddBottom, paddTop} from '../commons/models';
import {LogOut} from './RegCafes';

export default ({navigation: {navigate}, route: {params}}) => {
  const {profile, myid, name, fname, recallApply, logout} = useAContext(),
    {status, cafe: profCafe, phone} = profile,
    applied = status == 'applied',
    init = params?.cafe || profCafe,
    cafeID = init.id,
    [cafe, setCafe] = useState(init),
    {name: title, net, firstApply} = cafe,
    hasAdmin = firstApply && firstApply != myid;

  useEffect(() => {
    db.doc('cafes/' + cafeID)
      .get()
      .then(dc => setCafe({...dc.data(), id: dc.id}));
  }, [cafeID]);

  if (cafe.id != cafeID || !title)
    // когда админ обновляет статус, cafe состоит только из id
    return (
      <Container>
        <Loader />
        <Footer />
      </Container>
    );

  return (
    <Container>
      <StatusBar
        barStyle="light-content"
        backgroundColor={!applied ? '#000' : '#ff5e00'}
      />

      <Body style={!applied && {backgroundColor: 'black'}}>
        <NavRow>{!applied && <LogOut white {...{logout}} />}</NavRow>

        <View>
          <Image source={applied ? smileIcon : sadIcon} />
          <Title style={{color: 'white'}}>
            {applied
              ? 'Заявка отправлена!'
              : status == 'denied'
              ? title + '  отказали вам в доступе'
              : 'Заявка отменена'}
          </Title>
          {status != 'denied' && (
            <Status>
              {status == 'cancelled'
                ? 'Вы отменили заявку в ' + title
                : 'Вы отправили заявку ' + //  значит статус 'applied'
                  (hasAdmin
                    ? `в ${title} от имени ${name + ' ' + fname}. Ждем ответа`
                    : `на подключение ${
                        (net ? 'сети ' : '') + title
                      }. Мы свяжемся с вами по тел. ${phone}`)}
            </Status>
          )}
          <Button
            big
            white
            transp={applied}
            onPress={() =>
              applied ? recallApply(hasAdmin) : navigate('RegCafes')
            }
            text={applied ? 'Отменить заявку' : 'К списку заведений'}
            style={{marginTop: 24}}
          />
        </View>
      </Body>

      <Footer />
    </Container>
  );
};

const Footer = () => (
  <FooterView>
    <Title>Наши контакты</Title>
    <Text style={{marginTop: 16}}>Есть вопросы? Напишите нам:</Text>
    <View style={{marginTop: 20}}>
      <ContactPress
        onPress={() => Linking.openURL('https://t.me/ivanonishenko')}>
        <Telegram />
        <Contact>@ivanonishenko</Contact>
      </ContactPress>
      <ContactPress
        onPress={() => Linking.openURL('mailto:ollo.manage@gmail.com')}>
        <EmailIcon />
        <Contact>ollo.manage@gmail.com</Contact>
      </ContactPress>
    </View>
  </FooterView>
);

const Touch = styled.TouchableOpacity``;
const Press = styled.Pressable``;

const Row = styled.View`
  flex-direction: row;
`;

const Container = styled.View`
  flex: 1;
  background: white;
`;

const Body = styled.View`
  flex: 1;
  justify-content: space-between;
  background: #ff5e00;
  padding: 24px 16px;
  padding-top: ${24 + paddTop}px;
`;

const NavRow = styled(Row)`
  justify-content: flex-end;
  margin-top: -24px;
`;

const Image = styled.Image`
  width: 100px;
  height: 100px;
  margin: -16px;
  margin-bottom: 0;
`;

const Text = styled.Text`
  font-family: 'Rubik-Regular';
  font-size: 16px;
  line-height: 24px;
  flex-shrink: 1;
`;

const Status = styled(Text)`
  color: white;
  margin-top: 16px;
`;

const FooterView = styled.View`
  justify-content: flex-end;
  padding: 24px 16px;
  padding-bottom: ${24 + paddBottom}px;
`;

const ContactPress = styled(Press)`
  flex-direction: row;
  align-items: center;
  padding: 8px 0;
  /* background: red; */
`;

const Contact = styled(Text)`
  font-family: 'Rubik-Medium';
  margin-left: 8px;
  text-decoration-line: underline;
`;
