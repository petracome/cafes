import React, {useState, useEffect, useCallback, useRef} from 'react';
import {
  Keyboard,
  FlatList,
  Linking,
  StatusBar,
  RefreshControl,
} from 'react-native';
import styled from 'styled-components/native';
import fire from '@react-native-firebase/firestore';
const db = fire();
import orderBy from 'lodash/orderBy';
import useAContext from '../commons/AuthContext';
import {paddTop, paddBottom} from '../commons/models';
import {Title, RemoveIcon, Loader, BackTouch} from '../commons/IconsForms';

export default ({navigation: {navigate, push, goBack}}) => {
  const {status, cafe: init, logout} = useAContext(),
    [all, setAll] = useState([]),
    [results, setResults] = useState(all),
    [search, setSearch] = useState(''),
    searchref = useRef(),
    timerHandler = useRef(),
    clearSearch = () => (
      setSearch(''), searchref?.current.focus(), setResults(all)
    );

  const getCafes = async () => {
    await db
      .collection('cafes')
      .get()
      .then(async query => {
        let arr = [];
        query.forEach(async dc => {
          let {status, name, breaks, lunches} = dc.data();
          status &&
            name &&
            (breaks || lunches) &&
            arr.push({...dc.data(), id: dc.id});
        });
        let sorted = orderBy(arr, 'name');
        setAll(sorted), setResults(sorted);
      });
  };

  useEffect(() => {
    getCafes();
  }, []);

  const onChangeSearch = useCallback(
    inp => {
      clearTimeout(timerHandler.current);
      let cach = inp;
      timerHandler.current = setTimeout(() => {
        setSearch(cach);
        const trim = cach.trim();
        if (trim.length != 1)
          setResults(
            trim.length == 0
              ? all
              : all.filter(({name}) =>
                  name.toLowerCase().includes(trim.toLowerCase()),
                ),
          );
      }, 500);
    },
    [results], // без этого не ререндерит results
  );

  const renderCafe = ({item: cafe}) => (
    <Row>
      <Touch
        onPress={() =>
          init?.id == cafe.id ? navigate('RegStatus') : push('RegForm', {cafe})
        }
        activeOpacity={0.5}>
        <Cafe>{cafe.name}</Cafe>
      </Touch>
    </Row>
  );

  return (
    <Container>
      <StatusBar barStyle="light-content" backgroundColor="#ff5e00" />
      <Header onPress={Keyboard.dismiss}>
        {status && (
          <NavRow>
            <BackTouch onPress={goBack} color="white" />
            <LogOut white {...{logout}} />
          </NavRow>
        )}

        <Title style={{color: 'white'}}>Выберите ваше заведение</Title>
        <InputRow>
          <Input
            placeholder="Найти по названию"
            defaultValue={search}
            onChangeText={onChangeSearch}
            returnKeyType="search"
            placeholderTextColor="#888"
            ref={searchref}
          />
          {search.length > 1 && <RemoveIcon onPress={clearSearch} />}
        </InputRow>
      </Header>

      <FlatList
        data={results}
        keyExtractor={({id}) => id}
        renderItem={renderCafe}
        ListHeaderComponent={
          !all[0] ? (
            <Loader />
          ) : (
            <Touch
              onPress={() => Linking.openURL('ollo.ua')}
              activeOpacity={0.5}>
              <Row>
                <QuestIcon />
                <Caption>Вашего заведения нет в списке?</Caption>
              </Row>
            </Touch>
          )
        }
        keyboardShouldPersistTaps="handled"
        keyboardDismissMode="on-drag"
        refreshControl={
          <RefreshControl
            refreshing={false}
            onRefresh={getCafes}
            tintColor="#ff5e00"
          />
        }
        contentContainerStyle={{
          padding: 8,
          paddingHorizontal: 16,
          paddingBottom: 16 + paddBottom,
        }}
      />
    </Container>
  );
};

export const LogOut = ({white, text, logout}) => (
  <Touch onPress={logout} activeOpacity={0.5}>
    <Caption style={white && {color: 'white'}}>
      {text || 'Сменить аккаунт'}
    </Caption>
  </Touch>
);

const Touch = styled.TouchableOpacity``;
const Press = styled.Pressable``;

const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;

const Container = styled.View`
  flex: 1;
  background-color: white;
`;

const Header = styled(Press)`
  background: #ff5e00;
  padding: 16px;
  padding-top: ${24 + paddTop}px;
`;

const NavRow = styled(Row)`
  justify-content: space-between;
  margin: -24px 0 4px -16px;
`;

const Text = styled.Text`
  font-family: 'Rubik-Regular';
  font-size: 16px;
  color: black;
  flex-shrink: 1;
`;

const InputRow = styled(Row)`
  background: #fff;
  border-radius: 8px;
  overflow: hidden;
  height: 40px;
  margin-top: 16px;
`;

const Input = styled.TextInput`
  font-family: 'Rubik-Regular';
  font-size: 16px;
  flex: 1;
  padding: 8px 12px;
`;

const Cafe = styled(Text)`
  line-height: 24px;
  padding: 8px 0;
`;

const Caption = styled(Cafe)`
  font-family: 'Rubik-Medium';
`;

const QuestIcon = () => (
  <Circle>
    <Quest>?</Quest>
  </Circle>
);

const Circle = styled.View`
  width: 19px;
  height: 19px;
  border-radius: 10px;
  border: 2px solid black;
  margin-right: 14px;
`;

const Quest = styled.Text`
  font-family: 'Rubik-Medium';
  font-size: 13px;
  line-height: 14px;
  color: #000;
  text-align: center;
`;
