import React from 'react';
import {View, StatusBar, ActivityIndicator} from 'react-native';
import styled from 'styled-components';
import ImageViewer from 'react-native-image-zoom-viewer';

export default ({
  route: {
    params: {url},
  },
  navigation: {goBack},
}) => (
  <BackView onPress={goBack}>
    <StatusBar backgroundColor="rgba(0, 0, 0, 0.75)" />
    <ImageViewer
      backgroundColor="rgba(0,0,0,0)"
      imageUrls={[{url}]}
      enableSwipeDown={true}
      swipeDownThreshold={100}
      renderIndicator={() => null}
      loadingRender={() => <ActivityIndicator size="large" color="#fff" />}
      renderFooter={() => <Text>Смахните вниз, чтобы закрыть</Text>}
      footerContainerStyle={{width: '100%'}}
      onCancel={goBack}
    />
  </BackView>
);

const Press = styled.Pressable``;

const BackView = styled(Press)`
  flex: 1;
  background-color: rgba(0, 0, 0, 0.75);
`;

const Text = styled.Text`
  font-family: 'Rubik-Regular';
  text-align: center;
  color: white;
  font-size: 12px;
  padding: 18px 0;
`;
