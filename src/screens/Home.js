import React, {useRef} from 'react';
import {FlatList, RefreshControl, StatusBar} from 'react-native';
import styled from 'styled-components';
import usecontext from '../commons/CafeContext';
import useMContext from '../commons/MenuContext';
import {wwidth as width, paddTop, paddBottom} from '../commons/models';
import {Loader} from '../commons/IconsForms';
import Header from '../comp/HomeHead';
import Post from '../comp/Post';
import Footer from '../comp/HomeFooter';

export default ({navigation: {navigate, replace}}) => {
  const {cafe} = usecontext(),
    menuTypes = ['breaks', 'lunches'].filter(t => cafe[t]),
    listref = useRef();

  if (!cafe) return <Loader />;
  return (
    <Container>
      <StatusBar barStyle="dark-content" backgroundColor="#fff" />
      <FlatList
        data={[
          <Header {...{cafe, listref, navigate}} />,
          <FlatList
            ref={listref}
            data={menuTypes.map(type => (
              <MenuGroup {...{type, navigate}} />
            ))}
            renderItem={({item}) => item}
            keyExtractor={(_, i) => i.toString()}
            initialNumToRender={1}
            scrollEnabled={false}
            pagingEnabled={true}
            onScrollToIndexFailed={() => {}}
            horizontal
            showsHorizontalScrollIndicator={false}
          />,
        ]}
        renderItem={({item}) => item}
        keyExtractor={(_, i) => i.toString()}
        refreshControl={
          <RefreshControl
            refreshing={false}
            onRefresh={() => replace('Home')}
            tintColor="#ff5e00"
          />
        }
      />

      <Footer {...{navigate, menuTypes}} />
    </Container>
  );
};

const MenuGroup = React.memo(({type, navigate}) => {
  const {menu, updatePostField: update, delpost} = useMContext(),
    renderPost = ({item: it, index: ind}) => (
      <Post {...{it, type, ind, update, delpost, navigate}} />
    );
  if (!menu) return <Loader />;
  if (!menu[type]) return null;
  if (!menu[type][0])
    return (
      <ZeroMenuView>
        <Press onPress={() => navigate('Edit', {type})}>
          <AddTitle>
            Добавьте ваш первый {type == 'breaks' ? 'завтрак' : 'обед'}!
          </AddTitle>
          <AddText>Здесь будут ваши публикации</AddText>
        </Press>
      </ZeroMenuView>
    );

  return (
    <FlatList
      data={menu[type]}
      keyExtractor={({time}, i) => (time || i).toString()}
      renderItem={renderPost}
      ItemSeparatorComponent={MenuSeparate}
      scrollEnabled={false}
      style={{width}}
      contentContainerStyle={{paddingBottom: 64 + paddBottom}}
    />
  );
  // Если группирвоать посты по активности
  // const groupedByStatus = groupBy(menu[t], 'active'),
  //  keys = Object.keys(groupedByStatus).sort().reverse();
  // const renderStatusGroupes = ({item: key}) => (<FlatList  data={groupedByStatus[key]} ListHeaderComponent={<Title>{key == 'true' ? 'Активные' :  'Отключенные'}</Title>}
  // return (<FlatList data={keys} renderItem={renderStatusGroupes}  keyExtractor={it => it}  ItemSeparatorComponent={() => <View style={{height: 16}} />}  contentContainerStyle={{paddingVertical: 16}}  style={{width: width}} scrollEnabled={false} /> );
});

const Press = styled.Pressable``;
const Container = styled.View`
  flex: 1;
  background: white;
  padding-top: ${paddTop}px;
`;

const MenuSeparate = styled.View`
  height: 1px;
  background: #e7e7e7;
  margin: 0 16px;
`;

const ZeroMenuView = styled.View`
  width: ${width}px;
  padding: 24px 16px;
`;

const AddTitle = styled.Text`
  font-family: 'Rubik-Medium';
  font-size: 16px;
  line-height: 24px;
  text-align: center;
`;

const AddText = styled(AddTitle)`
  font-family: 'Rubik-Regular';
  color: #888;
  margin-top: 8px;
`;
