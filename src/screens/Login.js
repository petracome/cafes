import React from 'react';
import {View} from 'react-native';
import styled from 'styled-components/native';
import useContext from '../commons/AuthContext';
import {paddBottom, Logo} from '../commons/models';
import {BlankView, SocButtons, AbsLoader} from '../commons/IconsForms';

export default () => {
  const {appleLogin, googleLogin, initializing} = useContext();

  return (
    <Container>
      <BlankView>
        <Logo />
        <LogoCaption>Заведениям</LogoCaption>
      </BlankView>

      <SocButtons
        {...{appleLogin, googleLogin}}
        style={{padding: 16, paddingBottom: 24 + paddBottom}}
      />

      {initializing && <AbsLoader />}
    </Container>
  );
};

const Container = styled.View`
  flex: 1;
  background-color: #fff;
`;

const LogoCaption = styled.Text`
  font-family: 'Rubik-Regular';
  font-size: 16px;
  line-height: 24px;
  margin-top: 8px;
`;
