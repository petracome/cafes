import React, {useState, useEffect} from 'react';
import {FlatList, ActivityIndicator, View, RefreshControl} from 'react-native';
import styled from 'styled-components/native';
import fire from '@react-native-firebase/firestore';
const db = fire();
//import orderBy from 'lodash/orderBy';
import useAContext from '../commons/AuthContext';
import {paddTop, isIOS, paddBottom} from '../commons/models';
import {
  Title,
  BackTouch,
  Loader,
  SocIcon,
  AbsLoader,
} from '../commons/IconsForms';
import StaffCard, {Name, Email} from '../comp/StaffCard';
import {Image} from '../comp/HomeHead';
import {LogOut} from './RegCafes';
const usersdb = db.collection('users');

export default ({navigation: {goBack, replace}}) => {
  const {
      profile: {name, photo, email, provider},
      role,
      logout,
      cafeID,
    } = useAContext(),
    dbstaff = db.doc('staff/' + cafeID),
    isadmin = role == 'admin',
    [staff, setStaff] = useState(),
    [load, setLoad] = useState(false),
    {applied, approved} = staff || {},
    hasApplies = applied && applied[0];

  const getstaff = async () =>
    await dbstaff.get().then(async dc => {
      let {applied: apls, approved: aprs} = dc.data(),
        obj = await getUsers(isadmin ? aprs.concat(apls) : aprs),
        approved = aprs.map(id => obj[id]);
      setStaff(
        isadmin ? {approved, applied: apls.map(id => obj[id])} : {approved},
      ),
        setLoad(false);
    });

  useEffect(() => {
    getstaff();
  }, [role]);

  const updateApply = async (i, status, role) => {
    setLoad(true);
    let user = {...applied[i], status, role};
    await updateDBstaff(cafeID, user, true);
    setStaff(pr => {
      let apls = [...pr.applied];
      apls.splice(i, 1);
      return status == 'approved'
        ? {applied: apls, approved: [...pr.approved, user]}
        : {...pr, applied: apls};
    });
    setLoad(false);
  };

  const updateStaff = async (i, status, role) => {
    setLoad(true);
    let user = {...approved[i], status, role},
      denied = status == 'denied';
    await updateDBstaff(cafeID, user);
    setStaff(pr => {
      let aprs = denied
        ? [...pr.approved]
        : [...pr.approved.slice(0, i), user, ...pr.approved.slice(i + 1)];
      denied && aprs.splice(i, 1);
      return {...pr, approved: aprs};
    });
    setLoad(false);
  };

  useEffect(() => {
    if (isadmin && staff) {
      return dbstaff.onSnapshot(async dc => {
        let {applied: apls} = dc.data();
        if (!applied || applied.length != apls.length) {
          setLoad(true);
          let obj = await getUsers(apls);
          setStaff(pr => ({...pr, applied: apls.map(id => obj[id])}));
          setLoad(false);
        }
      });
    }
  }, [role, staff]);

  const renderGroups = ({item: key, index: i}) =>
    !staff && i == 0 ? (
      <Loader />
    ) : staff && (i == 1 || (isadmin && hasApplies)) ? (
      <FlatList
        data={staff[key]}
        keyExtractor={({email}) => email}
        renderItem={i == 0 ? renderApplied : renderApproved}
        scrollEnabled={false}
        ListHeaderComponent={
          <TitleView>
            <Title>{i == 0 ? 'Новые заявки' : 'Сотрудники'}</Title>
          </TitleView>
        }
      />
    ) : null;

  const renderApplied = ({item: it, index: ind}) => (
      <StaffCard {...{it, ind}} applied update={updateApply} />
    ),
    renderApproved = ({item: it, index: ind}) => (
      <StaffCard {...{it, ind}} update={updateStaff} />
    );

  return (
    <Container>
      <NavRow>
        <BackTouch onPress={goBack} />
        <LogOut text="Выйти" {...{logout}} />
      </NavRow>

      {/* {staff && !load && ( */}
      <FlatList
        data={['applied', 'approved']}
        keyExtractor={(_, i) => i.toString()}
        renderItem={renderGroups}
        ListHeaderComponent={
          <ProfileRow>
            <UserPic {...{photo, name}} />
            <View>
              <Name numberOfLines={1}>{name}</Name>
              <Row>
                <SocIcon {...{provider}} />
                <Email numberOfLines={1}>{email}</Email>
              </Row>
            </View>
          </ProfileRow>
        }
        refreshControl={
          <RefreshControl
            refreshing={false}
            onRefresh={() => replace('Staff')}
            tintColor="#ff5e00"
          />
        }
      />
      {/* )} */}

      {load && <AbsLoader />}
    </Container>
  );
};

const getUsers = async ids => {
  try {
    if (!ids[1]) {
      let doc = await usersdb.doc(ids[0]).get();
      return {[ids[0]]: doc.data()};
    } else {
      let obj = {};
      await usersdb
        .where('email', 'in', ids)
        .get()
        .then(query => query.forEach(dc => (obj[dc.id] = dc.data())));
      return obj;
    }
  } catch (err) {
    alert(
      'Не получилось загрузить список сотрудников, проверьте доступ в интернет и попробуйте еще раз.\n' +
        err.toString(),
    );
  }
};

const updateDBstaff = async (cafeID, user, isapply) => {
  let {email: id, role, status} = user,
    userdb = db.doc('users/' + id),
    staffdb = db.doc('staff/' + cafeID),
    approved = isapply && status == 'approved',
    denied = !isapply && status == 'denied',
    batch = db.batch();
  batch.update(userdb, {status, role: role || null, cafe: {id: cafeID}});
  isapply && batch.update(staffdb, {applied: fire.FieldValue.arrayRemove(id)});
  approved && batch.update(staffdb, {approved: fire.FieldValue.arrayUnion(id)});
  denied && batch.update(staffdb, {approved: fire.FieldValue.arrayRemove(id)});
  await batch.commit();
};

const Press = styled.Pressable``;

const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;

const Container = styled.View`
  flex: 1;
  background-color: white;
`;

const NavRow = styled(Row)`
  justify-content: space-between;
  padding: ${paddTop}px 16px 0 0;
`;

const TitleView = styled.View`
  border-bottom-width: 1px;
  border-bottom-color: #e7e7e7;
  padding: 40px 16px 9px;
`;

const ProfileRow = styled(Row)`
  padding: 8px 16px 0;
`;

const UserPic = ({photo, name}) =>
  photo ? (
    <Image source={{uri: photo}} />
  ) : (
    <BlankUser>
      <BlankUserChar>{name[0]}</BlankUserChar>
    </BlankUser>
  );

const BlankUser = styled.View`
    background-color: #59b7c9;
    width: 48px;
    height: 48px;
    border-radius: 25px;
    justify-content: center;
    align-items: center;
    margin-right: 12px;
  `,
  BlankUserChar = styled.Text`
    font-family: 'Rubik-SemiBold';
    font-size: 26px;
    color: #fff;
    text-transform: capitalize;
  `;
