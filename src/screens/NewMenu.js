import React, {useRef, useState} from 'react';
import {ScrollView, View} from 'react-native';
import styled from 'styled-components/native';
import useContext from '../commons/CafeContext';
import {
  paddTop,
  paddBottom,
  handleColon,
  onInputEnd,
  checkTimeInp as checkTime,
  checkMCostInp as checkCost1,
} from '../commons/models';
import {PageTitle, BackTouch, Button, AbsLoader} from '../commons/IconsForms';
import {FlexView, Caption, Input, InputRow} from '../comp/EditItem';

// console.log('+re0'.trim());

export default ({
  navigation: {goBack, navigate},
  route: {
    params: {type},
  },
}) => {
  const {updateFields} = useContext(),
    [startTime, setStartTime] = useState(),
    [endTime, setEndTime] = useState(),
    [minCost, setMinCost] = useState(),
    [maxCost, setMaxCost] = useState(null),
    [load, setLoad] = useState(false);

  const i1ref = useRef(),
    i2ref = useRef(),
    i3ref = useRef(),
    toi1ref = () => i1ref?.current.focus(),
    toi2ref = () => i2ref?.current.focus(),
    toi3ref = () => i3ref?.current.focus();

  const save = async () => {
    if (!checkTime(startTime) || !checkTime(endTime))
      return alert('Время должно быть в формате ХХ:ХХ');
    if (startTime == endTime)
      return alert('Время с и Время до должны различаться');
    if (!checkCost1(minCost)) return alert('Некорректная цена от');
    else {
      setLoad(true);
      await updateFields({
        [type]: {
          startTime,
          endTime,
          minCost,
          maxCost: maxCost || null,
          menu: [],
        },
      });
      setLoad(false), navigate('Edit', {type});
    }
  };

  return (
    <Container>
      <Row>
        <BackTouch onPress={goBack} />
      </Row>
      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
          padding: 16,
          paddingTop: 8,
          paddingBottom: 24 + paddBottom,
          justifyContent: 'space-between',
        }}>
        <View>
          <PageTitle>Добавьте расписание и цены</PageTitle>
          <Title>{type == 'breaks' ? 'Завтраки' : 'Обеды'}</Title>
          <View style={{marginRight: -16}}>
            <Row>
              <InputComp onEnd={setStartTime} i={0} next={toi1ref} />
              <InputComp onEnd={setEndTime} i={1} reff={i1ref} next={toi2ref} />
            </Row>
            <Row>
              <InputComp onEnd={setMinCost} i={2} reff={i2ref} next={toi3ref} />
              <InputComp onEnd={setMaxCost} i={3} reff={i3ref} />
            </Row>
          </View>
        </View>

        <Button big text="Сохранить" onPress={save} style={{marginTop: 32}} />
      </ScrollView>
      {load && <AbsLoader />}
    </Container>
  );
};

const InputComp = ({i, onEnd, next, reff}) => {
  const istime = i == 0 || i == 1,
    [value, setValue] = useState('');
  const onChangeText = tx =>
    setValue(pr => (!istime ? tx : handleColon(pr, tx)));
  const onEndEditing = () => {
    let fin = onInputEnd(value, istime);
    setValue(fin), onEnd(istime ? fin : parseInt(fin));
  };
  return (
    <FlexView>
      <Caption>
        {istime
          ? 'Время ' + (i == 0 ? 'c' : 'до')
          : 'Цена ' + (i == 2 ? 'от' : 'до') + ', UAH'}
      </Caption>
      <InputRow>
        <Input
          ref={reff}
          {...{value, onChangeText, onEndEditing}}
          maxLength={istime ? 5 : i == 2 ? 3 : 4}
          onSubmitEditing={next}
          returnKeyType="next"
          keyboardType="number-pad"
          blurOnSubmit={i == 3}
        />
      </InputRow>
    </FlexView>
  );
};

const Press = styled.Pressable``;
const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;
//KeyboardAvoidingView`
const Container = styled.View`
  flex: 1;
  background-color: white;
  padding-top: ${paddTop}px;
`;

const Title = styled.Text`
  font-family: 'Rubik-Medium';
  font-size: 16px;
  line-height: 24px;
  padding: 16px 0 8px;
`;
