import React, {useState} from 'react';
import {ScrollView, View, Keyboard, Alert} from 'react-native';
import styled from 'styled-components/native';
import FastImage from 'react-native-fast-image';
import stor from '@react-native-firebase/storage';
import IMpicker from 'react-native-image-crop-picker';
import useContext from '../commons/MenuContext';
import {wwidth as width, isIOS, paddTop, paddBottom} from '../commons/models';
import {
  Button,
  Switcher,
  BackTouch,
  TrashIcon,
  AbsLoader,
} from '../commons/IconsForms';
//import ImageResizer from 'react-native-image-resizer';
// import {Image} from 'react-native-compressor';

export default ({
  route: {
    params: {type, id, ind},
  },
  navigation: {navigate},
}) => {
  const goBack = () => navigate('Home'),
    {cafeID, menu, addPost, updatePost, delpost} = useContext(),
    isnew = !id && ind == undefined,
    currpost =
      !isnew && (!id ? menu[type][ind] : menu[type].find(p => p.id == id)),
    {
      photo: initp,
      desc: initd,
      active: inita,
      photoRatio: initw2h,
    } = currpost || {},
    typename =
      type == 'lunches' ? 'обед' : type == 'breaks' ? 'завтрак' : 'ужин';

  const [load, setLoad] = useState(false),
    [photoObj, setPhotoObj] = useState(
      isnew ? {} : {photo: initp, photoRatio: initw2h},
    ),
    {photo, photoRatio, size} = photoObj,
    [desc, setDesc] = useState(initd || ''),
    [active, setActive] = useState(isnew ? true : inita);

  const toggle = () => setActive(!active),
    checkdesc = () => (desc[0] ? desc.trim().length > 4 : true);

  // если удалить пост, сразу пропадает
  if (!isnew && !currpost) return <AbsLoader size="large" color="#FF5E00" />;

  const pickphoto = async () =>
    IMpicker.openPicker({
      // cropping: true,
      freeStyleCropEnabled: true,
      enableRotationGesture: false,
      cropperToolbarTitle: 'Изменить',
      writeTempFile: false,
    }).then(
      ({path, width: w, height: h, ...data}) =>
        setPhotoObj({...data, photo: path, photoRatio: w / h}),
      // ({path, width: w, height: h, size}) =>
      //   setPhotoObj({photo: path, height: (h / w) * width, size}),
      () => null,
    );

  const delphoto = () => setPhotoObj({});

  const remove = () =>
    Alert.alert(
      'Подтвердите удаление',
      'Удалить публикацию?',
      [
        {
          text: 'Да',
          onPress: async () => (
            setLoad(true), await delpost({type, id, ind}), goBack()
          ),
        },
        {text: 'Отмена', style: 'cancel'},
      ],
      {cancelable: true},
    );

  const nochange =
    (desc == initd || (!desc && !initd)) && photo == initp && active == inita;

  const save = async () => {
    Keyboard.dismiss();
    if (load || nochange) return;
    else if (!photo && !desc.trim()[0]) alert('Добавьте фото или описание');
    else if (!checkdesc()) alert('Описание не может быть таким коротким');
    else {
      setLoad(true);
      let ptime = Date.now(),
        pphoto =
          photo && photo != initp ? await loadphoto(ptime) : photo || null,
        post = {
          time: ptime,
          photo: pphoto,
          photoRatio: photoRatio || null,
          desc: desc.trim()[0] ? desc.trim() : null,
          active,
        };
      isnew
        ? (await addPost(type, post), navigate('Home', {type}))
        : (await updatePost({id, type, ind, ...post}), goBack());
    }
  };

  const loadphoto = async ptime => {
    const imgref = stor().ref('/menu/' + cafeID + '/' + (id || ptime)),
      uploadUri = isIOS ? photo.replace('file://', '') : photo,
      task = imgref.putFile(uploadUri);
    let url;
    try {
      await task.then(
        async () => (url = await imgref.getDownloadURL()),
        () => setLoad(false),
      );
      return url;
    } catch (e) {
      setLoad(false);
      alert('Ошибка с загрузкой фото, попробуйте еще раз:\n' + e);
    }
  };

  // const scrollref = useRef(),
  // onContentSizeChange = (_, hg) => scrollref?.current.scrollToEnd();
  return (
    <Container behavior={isIOS ? 'padding' : null}>
      <NavRow>
        <BackTouch onPress={goBack} />
        <PageTitle>
          {isnew ? 'Новый ' + typename : 'Редактировать ' + typename}
        </PageTitle>
      </NavRow>

      <ScrollView
        // ref={scrollref}  {...{onContentSizeChange}}
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={{
          flexGrow: 1,
          paddingTop: 8,
          paddingBottom: 24 + paddBottom,
          justifyContent: 'space-between',
        }}>
        {/* <KeyboardAvoidingView behavior={isIOS ? 'padding' : null}> */}
        <View>
          {!photo && <Blankimg onPress={pickphoto} />}
          {photo && (
            <Press onPress={() => navigate('IMG', {url: photo})}>
              <FastImage
                source={{uri: photo}}
                style={{width, height: width / photoRatio}}>
                <DarkLayout>
                  <Touch onPress={!load && pickphoto} activeOpacity={0.5}>
                    <ButtonText style={load && {fontFamily: 'Rubik-Regular'}}>
                      {!load || photo == initp
                        ? 'Изменить фото'
                        : 'загружаю фото ' + Math.round(size / 1000) + 'КБ'}
                    </ButtonText>
                  </Touch>
                  <DelImgPress onPress={() => !load && delphoto()}>
                    <TrashIcon color="white" />
                  </DelImgPress>
                </DarkLayout>
              </FastImage>
            </Press>
          )}

          <Body>
            <Input
              value={desc}
              placeholder="Добавьте описание"
              onChangeText={setDesc}
              onEndEditing={checkdesc}
              placeholderTextColor="#888"
              multiline={true}
            />
            <Row>
              <Switcher {...{toggle, active}} />
            </Row>
          </Body>
        </View>

        <View style={{paddingHorizontal: 16}}>
          <Button
            big
            onPress={save}
            text={isnew ? 'Опубликовать' : 'Сохранить изменения'}
            style={nochange && {backgroundColor: 'rgba(0,0,0,0.1)'}}
          />
          {!isnew && (
            <Button
              big
              transp
              onPress={remove}
              text="Удалить"
              style={{marginTop: 16}}
            />
          )}
        </View>
        {/* </KeyboardAvoidingView> */}
      </ScrollView>
      {load && <AbsLoader />}
    </Container>
  );
};

const Blankimg = ({onPress}) => (
  <BlankView>
    <Touch {...{onPress}} activeOpacity={0.5}>
      <ButtonText style={{borderColor: '#888', color: 'black'}}>
        Выбрать фото
      </ButtonText>
    </Touch>
  </BlankView>
);

const Press = styled.Pressable``;
const Touch = styled.TouchableOpacity``;
const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;

const Container = styled.KeyboardAvoidingView`
  flex: 1;
  background: white;
`;

const NavRow = styled(Row)`
  padding-top: ${paddTop}px;
`;

const Text = styled.Text`
  font-family: 'Rubik-Regular';
  font-size: 14px;
`;

const PageTitle = styled(Text)`
  font-size: 17px;
`;

const Image = styled.ImageBackground`
  width: ${width}px;
`;

const DarkLayout = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  background: rgba(0, 0, 0, 0.5);
`;

const BlankView = styled.View`
  justify-content: center;
  align-items: center;
  background: #e7e7e7;
  width: ${width}px;
  height: ${width}px;
`;

const ButtonText = styled.Text`
  font-family: 'Rubik-Medium';
  font-size: 16px;
  line-height: 24px;
  color: white;
  border: 1px solid white;
  border-radius: 8px;
  padding: 12px 40px;
`;

const DelImgPress = styled(Press)`
  position: absolute;
  bottom: 0;
  right: 0;
  padding: 12px 10px;
`;

const Body = styled.View`
  padding: 16px 16px 38px;
`;

const Input = styled.TextInput`
  font-family: 'Rubik-Regular';
  font-size: 16px;
  line-height: 24px;
  border-bottom-width: 1px;
  border-bottom-color: #e7e7e7;
  padding: 16px 0;
  margin-bottom: 32px;
`;
