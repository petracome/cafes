import React, {useCallback, useState} from 'react';
import {ScrollView, View, RefreshControl, Alert} from 'react-native';
import styled from 'styled-components/native';
import {useIsFocused} from '@react-navigation/native';
import useContext from '../commons/CafeContext';
import useMContext from '../commons/MenuContext';
import {paddTop, isIOS, paddBottom} from '../commons/models';
import {PageTitle, BackTouch, Button, AbsLoader} from '../commons/IconsForms';
import InfoItem from '../comp/EditItem';

export default ({navigation: {goBack, navigate, replace}}) => {
  const {cafe, updateFields} = useContext(),
    {menu} = useMContext(),
    menuTypes = ['breaks', 'lunches'].filter(it => cafe[it]),
    missType = !menuTypes.includes('lunches') ? 'lunches' : 'breaks',
    missTName = missType == 'lunches' ? 'Обеды' : 'Завтраки',
    postlength = type => menu[type]?.length,
    focused = useIsFocused(),
    [load, setLoad] = useState(false);

  const updateMenu = useCallback(
    async (type, field, value) =>
      await updateFields({[`${type}.${field}`]: value}),
    [],
  );

  const delmenu = type =>
    Alert.alert(
      'Подтвердите действие',
      'Удалить раздел ' +
        (type == 'breaks' ? 'Завтраки' : 'Обеды') +
        (postlength(type) ? `, включая ${postlength(type)} публикации?` : '?'),
      [
        {
          text: 'Да',
          onPress: async () => {
            setLoad(true);
            await updateFields({[type]: null});
            setLoad(false);
          },
        },
        {text: 'Отмена', style: 'cancel'},
      ],
      {cancelable: true},
    );

  return (
    <Container behavior={isIOS ? 'padding' : null}>
      <Row>
        <BackTouch onPress={goBack} />
      </Row>
      <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={{
          padding: 16,
          paddingTop: 8,
          paddingBottom: 24 + paddBottom,
        }}
        refreshControl={
          <RefreshControl
            refreshing={false}
            onRefresh={() => replace('EditCafe')}
            tintColor="#ff5e00"
          />
        }>
        <PageTitle>Расписание и цены</PageTitle>

        {menuTypes.map(tp => {
          const update = async (fld, val) => await updateMenu(tp, fld, val);
          return (
            <InfoGroup key={tp}>
              <TitleRow>
                <Title>{tp == 'breaks' ? 'Завтраки' : 'Обеды'}</Title>
                {menuTypes.length > 1 && (
                  <Press onPress={() => delmenu(tp)}>
                    <Remove>удалить</Remove>
                  </Press>
                )}
              </TitleRow>
              <View style={{marginRight: -16}}>
                <Row>
                  <InfoItem i={0} {...{tp, cafe, update, focused}} />
                  <InfoItem i={1} {...{tp, cafe, update, focused}} />
                </Row>
                <Row>
                  <InfoItem i={2} {...{tp, cafe, update, focused}} />
                  <InfoItem i={3} {...{tp, cafe, update, focused}} />
                </Row>
              </View>
            </InfoGroup>
          );
        })}

        {menuTypes.length < 2 && (
          <InfoGroup>
            <Title>{missTName}</Title>
            <Button
              transp
              text={'Добавить ' + missTName.toLowerCase()}
              onPress={() => navigate('NewMenu', {type: missType})}
              style={{marginTop: 6}}
            />
          </InfoGroup>
        )}

        <InfoGroup>
          <Title>Типы кухни</Title>
          <Touch onPress={() => navigate('Kitchens')} activeOpacity={0.5}>
            <Kitchen>{cafe.kitchens.map(k => k).join(', ')}</Kitchen>
          </Touch>
        </InfoGroup>
      </ScrollView>
      {load && <AbsLoader />}
    </Container>
  );
};

const Touch = styled.TouchableOpacity``;
const Press = styled.Pressable``;
const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;

const Container = styled.KeyboardAvoidingView`
  flex: 1;
  background-color: white;
  padding-top: ${paddTop}px;
`;

const InfoGroup = styled.View`
  padding: 16px 0 8px;
`;

const TitleRow = styled(Row)`
  justify-content: space-between;
`;

const Title = styled.Text`
  font-family: 'Rubik-Medium';
  font-size: 16px;
  line-height: 24px;
  padding-bottom: 8px;
`;

const Remove = styled(Title)`
  font-size: 12px;
  color: #888;
`;

const Text = styled.Text`
  font-family: 'Rubik-Regular';
  font-size: 14px;
  flex-shrink: 1;
  color: black;
`;

const Kitchen = styled(Text)`
  padding: 6px 0;
`;
