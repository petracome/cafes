import React from 'react';
import {Image, Platform, Dimensions, PermissionsAndroid} from 'react-native';
import stor from '@react-native-firebase/storage';
import Svg, {G, Path, Defs, ClipPath} from 'react-native-svg';
import fire from '@react-native-firebase/firestore';
const db = fire();

export const {width: wwidth, height} = Dimensions.get('window'),
  isIOS = Platform.OS == 'ios',
  wheight = height, //- (isIOS ? 20 : 0),
  isnewPhone = isIOS && height / wwidth > 2,
  paddTop = isnewPhone ? 44 : isIOS ? 20 : 0,
  paddBottom = isnewPhone ? 12 : 0;

export const handleColon = (pr, tx) => {
  let prl = pr.length,
    txl = tx.length,
    ind = tx.indexOf(':'),
    hours = tx.slice(0, 2),
    mpars = parseInt(tx.slice(2));
  if (ind < 0) return prl > 0 && txl > 1 ? hours + ':' + tx.slice(2) : tx;
  if (ind > 2) return hours + ':' + mpars + (mpars < 10 ? '0' : '');
  if (ind == 2) return prl == 4 && txl == 3 ? hours : tx;
  else return '';
};

export const onInputEnd = (val, istime) =>
  istime && val[1] && !val[4]
    ? val + (val[3] ? '0' : val[2] ? '00' : ':00')
    : val;

export const checkTimeInp = val =>
  val?.length == 5 &&
  val.indexOf(':') == 2 &&
  val.lastIndexOf(':') == 2 &&
  parseInt(val) < 24 &&
  parseInt(val.slice(3)) < 60;

export const checkMCostInp = val => val && val > 9 && val < 1000;

export const menuphoto =
  'https://firebasestorage.googleapis.com/v0/b/ullo-9c5aa.appspot.com/o/menu%2F0.0cfzo2b1ra2?alt=media&token=94d8bde7-4ec9-4d32-87da-ccb56441e642';
export const breakf =
  'https://firebasestorage.googleapis.com/v0/b/ullo-9c5aa.appspot.com/o/menu%2F0.a0xwsxpziz?alt=media&token=a44b364f-62e1-4903-ab75-04eeb53b1a1a';

export const savestor = async obj => {
  const fileRef = stor().ref('/jsons/users.json');
  var blob = new Blob([JSON.stringify(obj)], {type: 'application/json'});
  const task = fileRef.put(blob);
  try {
    await task.then(async () => {
      let url = await fileRef.getDownloadURL();
      console.log('getDownloadURL', url);
    });
  } catch (e) {
    console.error('Ошибка с загрузкой в стор:\n' + e);
  }
};

export const backupDB = async () => {
  let obj = {cafes: {}, menu: {}, staff: {}, users: {}};
  (await db.collection('cafes').get()).forEach(dc => {
    obj.cafes[dc.id] = dc.data();
  });
  (await db.collection('menu').get()).forEach(dc => {
    obj.menu[dc.id] = dc.data();
  });
  (await db.collection('staff').get()).forEach(dc => {
    obj.staff[dc.id] = dc.data();
  });
  (await db.collection('users').get()).forEach(dc => {
    obj.users[dc.id] = dc.data();
  });
  const fileRef = stor().ref('/backupDB.json');
  var blob = new Blob([JSON.stringify(obj)], {type: 'application/json'});
  const task = fileRef.put(blob);
  try {
    await task.then(async () => {
      let url = await fileRef.getDownloadURL();
      console.log('backupDB = ', url);
    });
  } catch (e) {
    console.error('Ошибка с загрузкой в стор:\n' + e);
  }
};

const updatecafes = async () => {
  // const allrefs = await db.collection('restaurants');
  // const alldocs = await allrefs.get();
  // var count = 149;
  // alldocs.forEach(async dc => {
  //   let obj = dc.data();
  //   let {name, cost, location} = obj;
  //   // obj.cost = count;
  //   // obj.location = {lat, lng, latitude: lat, longitude: lng};
  //   obj.id = dc.id;
  //   count++;
  //   await allrefs.doc(obj.id).set(obj);
  //});
};

export const geoPermission = async () => {
  try {
    let permissed = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );
    if (permissed) return true;
    else {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        // {
        //   title: 'title',
        //   message: 'message',
        //   buttonNegative: 'Cancel',
        //   buttonPositive: 'OK',
        // },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) return true;
      else return false;
    }
  } catch (error) {
    console.log(error);
    return false;
  }
};

export const groupBy = (array, property) =>
  array.reduce((result, item) => {
    const value = item[property]; // Cafe 1
    (result[value] = result[value] || []).push(item);
    // let index = result.findIndex((item) => item.hasOwnProperty(value));
    // let newObj = {};
    // index > -1    ? result[index][value].push(item) : ((newObj[value] = [item]), result.push(newObj));
    return result;
  }, {});

export const imageRatio = async img => {
  if (!String(img).startsWith('http')) {
    let obj = Image.resolveAssetSource(img);
    const ratio =
      obj && obj.hasOwnProperty('width') ? obj.width / obj.height : null;
    return ratio;
  } else if (String(img).startsWith('http')) {
    await Image.getSize(
      img,
      (wh, hh) => {
        const ratio = hh / wh;
        return ratio;
      },
      error => console.log(error),
    );
  } else return null;
};

export const customMapStyle = [
  {
    elementType: 'geometry',
    stylers: [
      {
        color: '#F4F6F8',
      },
    ],
  },
  {
    elementType: 'labels.icon',
    stylers: [
      {
        visibility: 'off',
      },
    ],
  },
  {
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#616161',
      },
    ],
  },
  {
    elementType: 'labels.text.stroke',
    stylers: [
      {
        color: '#F4F6F8',
      },
    ],
  },
  {
    featureType: 'administrative.land_parcel',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#bdbdbd',
      },
    ],
  },
  {
    featureType: 'poi',
    elementType: 'geometry',
    stylers: [
      {
        color: '#eeeeee',
      },
    ],
  },
  {
    featureType: 'poi',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#757575',
      },
    ],
  },
  {
    featureType: 'poi.park',
    elementType: 'geometry',
    stylers: [
      {
        color: '#e5e5e5',
      },
    ],
  },
  {
    featureType: 'poi.park',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#9e9e9e',
      },
    ],
  },
  {
    featureType: 'road',
    elementType: 'geometry',
    stylers: [
      {
        color: '#ffffff',
      },
    ],
  },
  {
    featureType: 'road.arterial',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#757575',
      },
    ],
  },
  {
    featureType: 'road.highway',
    elementType: 'geometry',
    stylers: [
      {
        color: '#dadada',
      },
    ],
  },
  {
    featureType: 'road.highway',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#616161',
      },
    ],
  },
  {
    featureType: 'road.local',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#9e9e9e',
      },
    ],
  },
  {
    featureType: 'transit.line',
    elementType: 'geometry',
    stylers: [
      {
        color: '#e5e5e5',
      },
    ],
  },
  {
    featureType: 'transit.station',
    elementType: 'geometry',
    stylers: [
      {
        color: '#eeeeee',
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'geometry',
    stylers: [
      {
        color: '#c9c9c9',
      },
    ],
  },
  {
    featureType: 'water',
    elementType: 'labels.text.fill',
    stylers: [
      {
        color: '#9e9e9e',
      },
    ],
  },
];

export const Logo = props => (
  <Svg
    width={82}
    height={81}
    viewBox="0 0 82 81"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}>
    <Path
      d="M30.574 0h7.479v80.766h-7.479V0zM43.947 0h7.479v80.766h-7.479V0z"
      fill="#FF5E00"
    />
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M69.66 0c-6.814 0-12.339 5.524-12.339 12.34v56.087c0 6.815 5.525 12.34 12.34 12.34 6.814 0 12.339-5.525 12.339-12.34V12.339C82 5.524 76.475 0 69.66 0zm0 7.665a3.365 3.365 0 00-3.364 3.366v58.705a3.365 3.365 0 106.73 0V11.03a3.365 3.365 0 00-3.365-3.366zM12.34 0C5.523 0 0 5.524 0 12.34v56.087c0 6.815 5.524 12.34 12.34 12.34 6.814 0 12.339-5.525 12.339-12.34V12.339C24.679 5.524 19.154 0 12.339 0zm0 7.665a3.365 3.365 0 00-3.366 3.366v58.705a3.365 3.365 0 106.73 0V11.03a3.365 3.365 0 00-3.365-3.366z"
      fill="#FF5E00"
    />
  </Svg>
);

export const Mark = props => (
  <Svg
    width={11}
    height={37}
    viewBox="0 0 11 37"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}>
    <G clipPath="url(#prefix__clip0)">
      <Path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M3.937.002H0v30.735a5.443 5.443 0 1010.887 0V.002H6.95v31.236a1.507 1.507 0 11-3.014 0V.002z"
        fill="#000"
      />
    </G>
    <Defs>
      <ClipPath id="prefix__clip0">
        <Path fill="#fff" d="M0 0h11v36.422H0z" />
      </ClipPath>
    </Defs>
  </Svg>
);
