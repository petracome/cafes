import React, {createContext, useContext, useState, useEffect} from 'react';
import auth from '@react-native-firebase/auth';
import fire from '@react-native-firebase/firestore';
const db = fire();
import {appleAuth} from '@invertase/react-native-apple-authentication';
import {
  GoogleSignin,
  statusCodes,
} from '@react-native-google-signin/google-signin';
// import messaging from '@react-native-firebase/messaging';

const AuthContext = createContext();
export default () => useContext(AuthContext);

export const AuthProvider = ({children}) => {
  const [initializing, setInitializing] = useState(false),
    [profile, setProfile] = useState(),
    {name: regnm, email, photo, cafe, status, role, time} = profile || {},
    myid = email,
    cafeID = cafe?.id;

  const space = regnm?.indexOf(' ', 2),
    name = space > 0 ? regnm.substring(0, space) : regnm,
    fname = space > 0 ? regnm.substring(space).trim() : '';

  const dbref = db.doc('users/' + myid);

  const addDBUser = async data => {
    let user = {...data, token: null, cafe: null, status: null};
    await db.doc('users/' + data.email).set(user);
    setProfile(user);
  };

  const getDBUser = async id => {
    const resp = (await db.doc('users/' + id).get()).data();
    resp && setProfile(resp);
  };

  const getStatus = async () => {
    const resp = (await dbref.get()).data();
    resp.status != status &&
      setProfile(prev => ({...prev, status: resp.status}));
  };

  const appleLogin = async () => {
    setInitializing(true);
    try {
      const appleResp = await appleAuth.performRequest({
          requestedOperation: appleAuth.Operation.LOGIN,
          requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
        }),
        {identityToken: token, nonce, fullName} = appleResp;
      if (token) {
        const creds = auth.AppleAuthProvider.credential(token, nonce),
          {user, additionalUserInfo} = await auth().signInWithCredential(creds);
        let {uid, email} = user,
          {isNewUser} = additionalUserInfo;
        if (!isNewUser) getDBUser(email);
        else {
          let {givenName: n1, familyName: n2} = fullName || {},
            name = n1 ? (n1 + ' ' + n2).trim() : 'без имени';
          addDBUser({uid, email, name, photo: '', provider: 'apple'});
        }
      } else {
        setInitializing(false);
        alert(
          'Ошибка со стороны Apple: не получили ваш токен. Попробуйте перезапустить приложение и повторить ',
        );
      }
    } catch (err) {
      setInitializing(false);
      err.code != appleAuth.Error.CANCELED &&
        alert(
          'Ошибка со стороны Apple, попробуйте перезапустить приложение и повторить.\n\n' +
            err.toString(),
        );
    }
  };

  const googleLogin = async () => {
    setInitializing(true);
    try {
      const {idToken} = await GoogleSignin.signIn(),
        creds = auth.GoogleAuthProvider.credential(idToken),
        {user, additionalUserInfo} = await auth().signInWithCredential(creds);
      let {uid, email, displayName: name, photoURL} = user,
        {isNewUser} = additionalUserInfo;
      if (!isNewUser) getDBUser(email);
      else {
        let photo = photoURL?.replace('=s96-c', '=s150') || '';
        addDBUser({uid, email, name, photo, provider: 'google'});
      }
    } catch (err) {
      setInitializing(false);
      err.code != statusCodes.SIGN_IN_CANCELLED &&
        alert(
          err.code == statusCodes.PLAY_SERVICES_NOT_AVAILABLE
            ? 'На телефоне устарели или отсутствуют Google Play Сервисы. Это необходимое приложение от Android, для обновления найдите его через Google-поиск на этом телефоне: «Сервисы Google Play» или «Google Play Services»'
            : 'Ошибка со стороны Google, можно попробовать перезапустить приложение и повторить.\n\n' +
                err.toString(),
        );
    }
  };

  const logout = async () => {
    await auth().signOut();
    setProfile(null);
  };

  const updateName = async nam => {
    let name = nam.trim();
    await updateFields({name});
  };

  const updatePhoto = async photo => await updateFields({photo});

  const updateNameNPhoto = async ({name: nam, photo}) => {
    let name = nam.trim();
    try {
      await dbref.update({photo, name});
      setProfile(prev => ({...prev, name, photo}));
    } catch (err) {
      alert(
        'Не получилось обновить ваш профиль, проверьте доступ в интернет и попробуйте еще раз.\n' +
          err.toString(),
      );
    }
  };

  const applyCafe = async (cafe, contact) => {
    let {id, hasAdmin} = cafe,
      {name: nm, fname: fn, phone: ph} = contact,
      name = nm.trim() + ' ' + fn.trim(),
      phone = ph && ph.trim()[0] ? ph.trim() : null,
      time = Date.now();
    hasAdmin
      ? await db.doc('staff/' + id).update({
          applied: fire.FieldValue.arrayUnion(myid),
        })
      : await db.doc('cafes/' + id).update({firstApply: myid, status: 'lead'});
    updateFields({cafe: {id}, name, phone, status: 'applied', time});
  };

  const recallApply = async hasAdmin => {
    hasAdmin
      ? await db.doc('staff/' + cafeID).update({
          applied: fire.FieldValue.arrayRemove(myid),
        })
      : await db
          .doc('cafes/' + cafeID)
          .update({firstApply: null, status: 'onmap'});
    updateFields({status: 'cancelled', role: null, time: Date.now()});
  };

  const updateToken = async () => {
    //     //ONLY FOR iOS, before updateToken:  // const requestUserPermission = async () => { const authStatus = await messaging().requestPermission(); const enabled =    authStatus === messaging.AuthorizationStatus.AUTHORIZED ||    authStatus === messaging.AuthorizationStatus.PROVISIONAL; //   if (enabled) {
    //     let token = await messaging().getToken();
    //     if (token && token != profile.token) {
    //       setProfile(prev => ({...prev, token}));
    //       await dbref.update({token});
    //       return token;
    //     } else !token && console.error('Failed fcm token');
  };

  useEffect(() => {
    initializing && myid && setInitializing(false);
    // myid && updateToken();
  }, [myid]);

  const updateFields = async obj => {
    await updateDB(obj);
    updateLocal(obj);
  };

  const updateDB = async obj => {
    let keys = Object.keys(obj);
    const batch = db.batch();
    keys.forEach(key => batch.update(dbref, {[key]: obj[key]}));
    await batch
      .commit()
      .catch(err =>
        alert(
          'Не получилось обновить, проверьте доступ в интернет и попробуйте еще раз.\n' +
            err.toString(),
        ),
      );
  };

  const updateLocal = obj => setProfile(prev => ({...prev, ...obj}));

  /// Подписываемся на изменения статуса status и role
  useEffect(() => {
    if (!myid) return;
    return dbref.onSnapshot(dc => {
      let {status: newst, role: newrl} = dc.data();
      (newst != status || newrl != role) &&
        updateLocal({status: newst, role: newrl});
    });
  }, [status, role]);

  return (
    <AuthContext.Provider
      value={{
        profile,
        setProfile,
        myid,
        email,
        name,
        fname,
        photo,
        status,
        role,
        time,
        cafe,
        cafeID,
        initializing,
        getDBUser,
        googleLogin,
        appleLogin,
        logout,
        updateName,
        updatePhoto,
        updateNameNPhoto,
        applyCafe,
        recallApply,
        getStatus,
      }}>
      {children}
    </AuthContext.Provider>
  );
};

const checkMail = async email => {
  let check;
  await auth()
    .fetchSignInMethodsForEmail(email)
    .then(resp => {
      console.log(resp);
      check = {isnew: resp.length < 1};
    });
  return check;
};
