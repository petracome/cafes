import React, {useState} from 'react';
import {
  View,
  TouchableNativeFeedback,
  Switch,
  ActivityIndicator,
} from 'react-native';
import styled from 'styled-components/native';
import {appleAuth} from '@invertase/react-native-apple-authentication';
import {AppleButton} from '@invertase/react-native-apple-authentication';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Feather from 'react-native-vector-icons/Feather';
import Entypo from 'react-native-vector-icons/Entypo';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Svg, {G, Path, Defs, Circle} from 'react-native-svg';
import {wwidth, wheight, isIOS} from '../commons/models';
const Touch = styled.TouchableOpacity``;
const Press = styled.Pressable``;
const Row = styled.View`
  flex-direction: row;
`;

export const Title = styled.Text`
  font-family: 'Rubik-Medium';
  font-size: 24px;
  line-height: 32px;
  flex-shrink: 1;
`;

export const PageTitle = styled(Title)`
  /* font-size: 26px;
  line-height: 34px; */
`;

export const SafeArea = ({children}) => <SafeAV>{children}</SafeAV>;

const SafeAV = styled.SafeAreaView`
  flex: 1;
  background: white;
`;

export const WwidthWrap = ({style, children}) => (
  <View style={[{width: wwidth, flex: 1}, style]}>{children}</View>
);

export const Filter = ({text, onPress, active, style}) => (
  <TouchFilter
    style={[
      active && {backgroundColor: '#000'},
      text.includes('▼') && {paddingRight: 14},
      style,
    ]}
    {...{onPress}}
    activeOpacity={0.5}>
    {text.includes('Фильтр') && <FilterIcon />}
    <FilterText style={active && {color: 'white'}}>{text}</FilterText>
  </TouchFilter>
);
const TouchFilter = styled(Touch)`
  flex-direction: row;
  align-items: center;
  justify-content: center;
  border-radius: 8px;
  border: 1px solid #e5edff;
  height: 30px;
  padding: 0 16px;
  margin-right: 8px;
`;
const FilterText = styled.Text`
  font-family: 'Rubik-Regular';
  font-size: 12px;
  color: black;
`;

export const Tabs = ({data, i, listref, tab, setTab, tabpress}) => {
  // const [tab, setTab] = useState(i || 0),
  // const tabpress = index => {
  //   setTab(index);
  //   listref?.current.scrollToIndex({index, animated: true});
  // };
  return (
    <TabsRow>
      {data.map((it, i) => {
        const onPress = () => tabpress(i);
        return (
          <Touch {...{onPress}} activeOpacity={0.5} key={it}>
            <TabView style={tab == i && {borderBottomWidth: 3}}>
              <TabText style={tab == i && {color: '#000'}}>{it}</TabText>
            </TabView>
          </Touch>
        );
      })}
    </TabsRow>
  );
};

const TabsRow = styled(Row)`
  border-bottom-width: 1px;
  border-bottom-color: #e7e7e7;
  padding: 0 16px 16px;
  margin-right: -16px;
  /* background: red; */
`;

const TabView = styled.View`
  flex-shrink: 1;
  border-bottom-color: #ff5e00;
  padding: 8px 0 4px;
  margin-right: 16px;
`;
const TabText = styled.Text`
  font-family: 'Rubik-Medium';
  font-size: 16px;
  line-height: 20px;
  color: #888;
`;

/// TextINPUTS
export const Buttons = ({apply, cancel, logout, cafe}) => (
  <View>
    {apply && (
      <Button
        onPress={apply}
        text={'Отправить заявку ' + (cafe.hasAdmin ? 'в ' : 'по ') + cafe.name}
        big
      />
    )}
    {cancel && <Button onPress={cancel} text="Отменить заявку" big />}
    <Button
      onPress={logout}
      text="Выйти из аккаунта"
      style={{marginTop: 8, backgroundColor: '#fff'}}
      color="black"
      big
    />
  </View>
);

export const SocButtons = ({appleLogin, googleLogin, style}) => (
  <View style={[{width: '100%'}, style]}>
    {
      //appleLogin && parseFloat(Platform.Version) >= 13 && (
      appleAuth.isSupported && (
        <AppleButton
          onPress={appleLogin}
          cornerRadius={8}
          style={[{width: '100%', height: 48, marginBottom: 16}, shadow16]}
        />
      )
    }
    <GoogleButton onPress={googleLogin} />
  </View>
);

export const GoogleButton = ({onPress}) => (
  <Button big white {...{onPress}} style={shadow16}>
    <GoogleIcon size={isIOS ? 13 : 16} marginRight={isIOS ? 6 : 12} />
    <GoogleText style={!isIOS && {fontFamily: 'Rubik-Medium'}}>
      {isIOS ? 'Вход с Google' : 'Войти через Google'}
    </GoogleText>
  </Button>
);

const GoogleText = styled.Text`
  font-size: ${isIOS ? 18 : 16}px;
  font-weight: 500;
`;

export const Button = ({text, color, transp, big, white, ...props}) => (
  <ButtonComp {...{big, white, transp, ...props}} activeOpacity={0.7}>
    {props.children || (
      <ButtonText
        style={{
          fontSize: big ? 16 : 14,
          color:
            color ||
            ((white && transp) || (!white && !transp) ? 'white' : 'black'),
        }}
        numberOfLines={1}>
        {text}
      </ButtonText>
    )}
  </ButtonComp>
);

const ButtonComp = ({transp, big, white, style, ...props}) =>
  !transp ? (
    <ButtonTouch
      style={[big && {height: 48}, white && {backgroundColor: 'white'}, style]}
      {...props}>
      {props.children}
    </ButtonTouch>
  ) : (
    <ButtonTouchTransp
      style={[big && {height: 48}, white && {borderColor: '#E7E7E7'}, style]}
      {...props}>
      {props.children}
    </ButtonTouchTransp>
  );

const ButtonTouch = styled(Touch)`
  flex-direction: row;
  justify-content: center;
  align-items: center;
  background: black;
  border-radius: 8px;
`;

const ButtonTouchTransp = styled(ButtonTouch)`
  background: rgba(0, 0, 0, 0);
  border: 1px solid #888;
`;

const ButtonText = styled.Text`
  font-family: 'Rubik-Medium';
  padding: 6px 16px;
`;

export const backgroundComponent = ({style}) => <BackView {...{style}} />;
const BackView = styled.View({
  backgroundColor: 'white',
  borderTopLeftRadius: 0,
  borderTopRightRadius: 0,
});

export const handleComponent = () => (
  //<HandleBack />
  <HandleLine />
  //</HandleBack>
);

const HandleBack = styled.View`
  height: 1px;
  align-items: center;
`;
const HandleLine = styled.View`
  position: absolute;
  align-self: center;
  top: 6px;
  width: 40px;
  height: 4px;
  background: #cfd4dc;
  border-radius: 2px;
`;

export const AbsLoader = () => (
  <Loader style={{position: 'absolute', height: wheight, elevation: 11}} />
);

/// ICONS
export const Loader = ({style}) => (
  <LoadView {...{style}}>
    <ActivityIndicator size="large" color="#ff5e00" />
  </LoadView>
);

const LoadView = styled.View`
  flex: 1;
  background: rgba(255, 255, 255, 0.75);
  justify-content: center;
  align-items: center;
  width: ${wwidth}px;
  padding: 40px;
`;

export const Switcher = ({toggle, active}) => (
  <Switch
    value={active}
    onValueChange={toggle}
    trackColor={{false: isIOS ? '#888' : '#e6dbd5', true: '#FF5E00'}}
    thumbColor={isIOS ? '#fff' : '#FF5E00'}
    ios_backgroundColor="#f5f5f5"
    style={!isIOS && {marginLeft: -4}}
  />
);

export const Telegram = () => <FontAwesome name="telegram" size={18} />;

export const EmailIcon = ({size, color}) => (
  <CircleView
    style={{
      width: size || 18,
      height: size || 18,
      backgroundColor: color || 'black',
    }}>
    <Email>@</Email>
  </CircleView>
);

const CircleView = styled.View`
  border-radius: 20px;
  justify-content: center;
  align-items: center;
`;

const Email = styled.Text`
  font-family: 'Rubik-SemiBold';
  font-size: 13px;
  color: white;
  margin: -1px -1px 0 0;
`;

export const MylocIcon = ({color}) => (
  // <MaterialIcons name="my-location" size={22} {...{color}} />
  <FontAwesome name="location-arrow" size={24} color={color || 'white'} />
);

export const CopyIcon = () => <Feather name="copy" size={30} />;

export const SocIcon = ({provider}) => (
  <FontAwesome
    name={provider}
    size={14}
    color={provider === 'google' ? '#de4d41' : '#A3AAAE'}
    style={{marginRight: 4}}
  />
);

export const EnterIcon = () => (
  <Feather name="corner-down-right" size={16} style={{marginTop: 7}} />
);

export const IconUP = () => (
  <Fontisto name="angle-up" color="#fac32d" size={16} />
);
export const IconDOWN = ({style}) => (
  <Fontisto {...{style}} name="angle-down" color="#d3d3d3" size={14} />
);

export const SettingsIcon = ({size, ...rest}) => (
  <Entypo name="dots-three-horizontal" size={size || 22} {...rest} />
);

export const CloseIcon2 = ({style, onPress}) => (
  <TouchableNativeFeedback {...{onPress}}>
    <View style={[{padding: 18}, style]}>
      <MaterialIcons name="close" size={24} />
    </View>
  </TouchableNativeFeedback>
);

export const BackTouch = ({onPress, color, style}) => (
  <TouchableNativeFeedback {...{onPress}}>
    <View style={[{padding: 12, paddingLeft: 22}, style]}>
      <MaterialIcons name="arrow-back-ios" size={23} color={color || 'black'} />
    </View>
  </TouchableNativeFeedback>
);

export const CookIcon = () => (
  <MaterialIcons
    name="timer"
    size={17}
    color="#b5b5b5"
    style={{marginTop: 2, marginRight: 5, marginLeft: -2}}
  />
);

export const RemoveIcon = ({onPress, style}) => (
  <Press {...{onPress, style}} activeOpacity={0.5}>
    <AntDesign
      name="closecircle"
      size={18}
      color="#000"
      style={{padding: 9, paddingHorizontal: 14}}
    />
  </Press>
);

export const TrashIcon = ({color, size, ...rest}) => (
  <Fontisto name="trash" color={color || 'black'} size={size || 22} {...rest} />
);
// MaterialIcons

export const SearchIcon = ({onPress, color, style}) => (
  <Touch {...{onPress, style}} activeOpacity={0.5}>
    <Feather name="search" size={25} {...{color}} />
  </Touch>
);

export const WarnIcon = () => (
  <AntDesign name="exclamationcircleo" size={15} style={{marginRight: 6}} />
);

export const RadioIcon = ({isactive}) =>
  !isactive ? <RadioView /> : <ActiveRadioView />;
const RadioView = styled.View`
  width: 16px;
  height: 16px;
  border-radius: 10px;
  border-width: 1px;
  border-color: #d3d3d3;
  margin: 2px 14px 0 0;
`;
const ActiveRadioView = styled(RadioView)`
  border-width: 5px;
  border-color: #fac32d;
`;

export const CheckboxIcon = ({active, style}) =>
  !active ? <CheckboxView {...{style}} /> : <CheckIcon {...{style}} />;
const CheckboxView = styled.View`
  width: 18px;
  height: 18px;
  border-radius: 4px;
  border-width: 1px;
  border-color: #d9dbe2;
  justify-content: center;
`;

const CheckIcon = ({style}) => (
  <Feather
    name="check"
    size={25}
    style={[{marginLeft: -2}, style]}
    color="#FF5E00"
  />
);

export const RightIcon = () => (
  <Entypo
    name="chevron-right"
    color="#d3d3d3"
    size={24}
    style={{marginRight: -5}}
  />
);

export const MapIcon = () => (
  <Entypo
    name="location" //"globe"
    color="#b9b9b9"
    size={14}
    style={{marginTop: -2, marginRight: 10}}
  />
);

export const InfoIcon = () => (
  <Entypo
    name="info-with-circle"
    color="#d3d3d3"
    size={20}
    style={{paddingRight: 18, paddingTop: 2}}
  />
);

export const FilterIcon = () => (
  <MaterialIcons name="filter-list" size={13} style={{marginTop: -3}} />
);

export const PhoneIcon = () => (
  <FontAwesome
    name="phone"
    size={19}
    color="#b5b5b5"
    style={{marginRight: 10}}
  />
);

export const MinusIcon = ({size, color}) => (
  <Entypo name="minus" size={size || 24} color={color || 'black'} />
);

export const PlusIcon = ({fill, ...props}) => (
  <Svg
    width={80}
    height={80}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}>
    <G filter="url(#prefix__filter0_d)">
      <Circle cx={40} cy={32} r={24} fill={fill || '#000'} />
      <Path
        d="M41 23a1 1 0 10-2 0v6a2 2 0 01-2 2h-6a1 1 0 100 2h6a2 2 0 012 2v6a1 1 0 102 0v-6a2 2 0 012-2h6a1 1 0 100-2h-6a2 2 0 01-2-2v-6z"
        fill="#fff"
      />
    </G>
    <Defs></Defs>
  </Svg>
);

export const CloseIcon = props => (
  <Svg
    width={80}
    height={80}
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}>
    <G filter="url(#prefix__filter0_d)">
      <Circle cx={40} cy={32} r={24} fill={'#e5e5e5'} />
      <Path
        d="M41 23a1 1 0 10-2 0v6a2 2 0 01-2 2h-6a1 1 0 100 2h6a2 2 0 012 2v6a1 1 0 102 0v-6a2 2 0 012-2h6a1 1 0 100-2h-6a2 2 0 01-2-2v-6z"
        fill="#000"
      />
    </G>
    <Defs></Defs>
  </Svg>
);

export const ClockIcon = () => (
  <Fontisto name="clock" size={22} color="#d3d3d3" style={{marginRight: 12}} />
);

export const AdrIcon = () => (
  <Entypo
    name="location-pin"
    size={32}
    color="#d3d3d3"
    style={{marginLeft: -5, marginRight: 5}}
  />
);

export const CartIcon = ({size, isFocused: foc}) => (
  <Feather
    name="shopping-bag"
    size={size || 17}
    color={foc ? '#FF5E00' : '#fff'}
  />
);
export const HomeIcon = ({isFocused: foc}) => (
  // <Feather name="menu" size={24} color={color || '#b5b5b5'} />
  <Entypo name="list" size={23} color={foc ? '#FF5E00' : '#fff'} />
);

export const UsersIcon = () => (
  <Feather
    name="users"
    size={23}
    style={{
      height: 34,
      padding: 6,
      marginRight: -10,
      paddingHorizontal: 16,
    }}
  />
);

export const UserIcon = ({isFocused: foc}) => (
  <Feather name="user" size={21} color={foc ? '#FF5E00' : '#fff'} />
);

export const InputIcons = ({onPress, onPress2}) => (
  <Row>
    <SendIcon onPress={onPress} style={!onPress2 && {paddingRight: 10}} />
    {onPress2 && <DelIcon onPress={onPress2} />}
  </Row>
);

export const SendIcon = props => (
  <SendTouch {...props} activeOpacity={0.5}>
    <SendView>
      <MaterialIcons name="send" size={16} color="#fff" />
    </SendView>
  </SendTouch>
);

const SendTouch = styled(Touch)`
  padding: 4px 2px 4px 10px;
  /* background-color: yellow; */
`;

const SendView = styled.View`
  background-color: black;
  border-radius: 18px;
  padding: 5px 4px 5px 6px;
`;

const DelTouch = styled(SendView)`
  padding: 5px 14px;
`;

export const EditIcon = () => (
  <MaterialIcons name="edit" size={16} style={{marginBottom: 2}} />
);

// ШАБЛОНЫ

export const shadow2 = {
  shadowOffset: {height: 2},
  shadowOpacity: 0.1,
  shadowRadius: 2,
  elevation: 2,
};

export const shadow16 = {
  shadowOpacity: 0.16,
  shadowOffset: {height: 8},
  shadowRadius: 16,
  elevation: 5,
};

export const BlankView = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
  /* background-color: #fff; */
`;

export const TextItem = styled.Text`
  font-family: 'Rubik-Regular';
  font-size: 14px;
  line-height: 17px;
  flex-shrink: 1;
`;

export const BlankText = styled(TextItem)`
  text-align: center;
`;

export const ItemView = ({item, onPress, ...rest}) => (
  <ItemRow {...rest}>
    <TextItem>
      {item}
      {onPress && <EditText onPress={onPress} />}
    </TextItem>
  </ItemRow>
);

const ItemRow = styled(Row)`
  flex: 1;
`;

export const EditText = ({...rest}) => (
  <TextEdit {...rest}>{'   изм.'}</TextEdit>
);
const TextEdit = styled(TextItem)`
  color: #c5c5c5;
  padding: 4px;
`;

/// CART

export const CartQuantity = ({quant, minus, plus, big}) =>
  !big ? (
    <QuantView>
      <MinusTouch onPress={minus}>
        <MinusIcon size={20} />
      </MinusTouch>
      <QuantText>{quant}</QuantText>
      <PlusTouch onPress={plus}>
        <PlusIcon size={20} />
      </PlusTouch>
    </QuantView>
  ) : (
    <QuantViewBIG>
      <MinusTouchBIG onPress={minus}>
        <MinusIcon />
      </MinusTouchBIG>
      <QuantTextBIG>{quant}</QuantTextBIG>
      <PlusTouchBIG onPress={plus}>
        <PlusIcon />
      </PlusTouchBIG>
    </QuantViewBIG>
  );

const QuantView = styled(Row)`
  justify-content: space-between;
  background-color: #fac32d;
  border-radius: 12px;
  width: 95px;
  height: 36px;
`;
const QuantViewBIG = styled(QuantView)`
  height: 44px;
  flex: 1;
  min-width: 110px;
  border-radius: 18px;
`;
const MinusTouch = styled(Touch)`
  flex: 1;
  padding: 8px;
  /* background-color: green; */
`;
const MinusTouchBIG = styled(MinusTouch)`
  padding: 12px;
`;
const PlusTouch = styled(MinusTouch)`
  align-items: flex-end;
`;
const PlusTouchBIG = styled(PlusTouch)`
  padding: 12px;
`;
const QuantText = styled.Text`
  font-family: 'medium';
  font-size: 16px;
`;
const QuantTextBIG = styled(QuantText)`
  font-size: 18px;
`;

export const smileIcon = {
  uri: 'https://firebasestorage.googleapis.com/v0/b/ullo-9c5aa.appspot.com/o/smile.png?alt=media&token=ea3d7fb2-f586-4ce1-bca4-8f387b09865b',
};

export const sadIcon = {
  uri: 'https://firebasestorage.googleapis.com/v0/b/ullo-9c5aa.appspot.com/o/sad.png?alt=media&token=b528bb4b-98e6-4481-8e66-571b0e031a16',
};

const GoogleIcon = props => (
  <Svg
    width={13}
    height={13}
    viewBox="0 0 13 13"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}>
    <Path
      d="M12.861 6.646a7.53 7.53 0 00-.103-1.23h-6.12V7.86h3.504a3.018 3.018 0 01-1.3 1.94v1.625h2.091c1.224-1.133 1.928-2.8 1.928-4.778z"
      fill="#4285F4"
    />
    <Path
      d="M6.638 13c1.755 0 3.223-.585 4.296-1.576L8.843 9.799c-.585.39-1.327.628-2.205.628-1.695 0-3.13-1.143-3.645-2.687H.837v1.674A6.49 6.49 0 006.638 13z"
      fill="#34A853"
    />
    <Path
      d="M2.992 7.74a3.772 3.772 0 01-.206-1.24c0-.433.076-.85.206-1.24V3.586H.836a6.425 6.425 0 000 5.828l2.156-1.673z"
      fill="#FBBC05"
    />
    <Path
      d="M6.638 2.573c.959 0 1.815.33 2.492.975l1.852-1.853C9.862.645 8.393 0 6.638 0A6.49 6.49 0 00.837 3.586L2.993 5.26c.514-1.544 1.95-2.687 3.645-2.687z"
      fill="#EA4335"
    />
  </Svg>
);
