import React, {createContext, useContext, useState, useEffect} from 'react';
import fire from '@react-native-firebase/firestore';
const db = fire();
import useAcontext from './AuthContext';

const CafeContext = createContext();
export default () => useContext(CafeContext);

export const CafeProvider = ({children}) => {
  const {cafe: profcafe, status} = useAcontext(),
    cafeID = profcafe?.id,
    [cafe, setCafe] = useState({}),
    {kitchens} = cafe;

  const dbref = db.doc('cafes/' + cafeID),
    getcafe = async () => await dbref.get().then(dc => setCafe(dc.data()));

  useEffect(() => {
    cafeID && status == 'approved' && getcafe();
  }, [cafeID, status]);

  const updateFields = async obj => {
    await updateDB(obj);
    updateLocal(obj);
  };

  const updateDB = async obj => {
    let keys = Object.keys(obj);
    const batch = db.batch();
    keys.forEach(key => batch.update(dbref, {[key]: obj[key]}));
    await batch
      .commit()
      .catch(err =>
        alert(
          'Не получилось обновить, проверьте доступ в интернет и попробуйте еще раз.\n' +
            err.toString(),
        ),
      );
  };

  const updateLocal = obj => {
    let key = Object.keys(obj)[0],
      ind = key.indexOf('.'),
      nested = ind > 0,
      [key1, key2] = nested ? [key.slice(0, ind), key.slice(ind + 1)] : [];
    setCafe(pr =>
      !nested
        ? {...pr, ...obj}
        : {...pr, [key1]: {...pr[key1], [key2]: obj[key]}},
    );
  };

  return (
    <CafeContext.Provider
      value={{cafeID, cafe, kitchens, getcafe, updateFields}}>
      {children}
    </CafeContext.Provider>
  );
};
