import React from 'react';
import {StackActions} from '@react-navigation/native';

export const navigationRef = React.createRef();

export default (name, params) => {
  navigationRef.current && navigationRef.current.navigate(name, params);
};

export function push(...args) {
  navigationRef.current &&
    navigationRef.current.dispatch(StackActions.push(...args));
}
