import React, {useRef} from 'react';
import {ScrollView, View} from 'react-native';
import styled from 'styled-components';
import Svg, {Path} from 'react-native-svg';
import usecontext from '../commons/CafesContext';
import {wwidth, wheight} from '../commons/models';
import {BackTouch, SearchIcon, RemoveIcon} from '../commons/IconsForms';
const brphoto =
  'https://firebasestorage.googleapis.com/v0/b/ullo-9c5aa.appspot.com/o/break.png?alt=media&token=709fc62a-a3f8-4a69-803b-559da56841ca';
const dinphoto =
  'https://firebasestorage.googleapis.com/v0/b/ullo-9c5aa.appspot.com/o/din.png?alt=media&token=ab0dbfd6-ed7d-4ebd-a05d-f933568d449e';

export const LogoRow = ({
  navigate,
  goBack,
  style,
  home,
  cafe,
  search,
  value,
  setValue,
  onchange,
}) => {
  const inputref = useRef();
  const clear = () => (setValue(''), inputref.current.focus());
  return (
    <LogoRowView {...{style}}>
      {!home && <BackTouch onPress={goBack} />}
      {!cafe && (
        <SearchPress onPress={() => !search && navigate('Search')}>
          <InputRow
            style={{marginLeft: !home ? 0 : 8, marginRight: !search ? 16 : 0}}>
            {!search && <SearchText>Поиск кафе и ресторанов</SearchText>}
            {search && (
              <>
                <Input
                  placeholder="по названию или адресу"
                  defaultValue={value}
                  onChangeText={onchange}
                  // onSubmitEditing={search}
                  returnKeyType="search"
                  autoFocus={true}
                  ref={inputref}
                  placeholderTextColor="#8d96a4"
                />
                {value.length > 1 && (
                  <RemoveIcon onPress={clear} style={{padding: 7}} />
                )}
              </>
            )}
          </InputRow>
        </SearchPress>
      )}
      {!search && <Logo />}
      {cafe && <View style={{width: 56}} />}
    </LogoRowView>
  );
};

export const Filters = ({navigate, list}) => {
  const {type, setType, pricing, types} = usecontext();
  const toggle = () => navigate('Filters');
  const hastypes = types.size > 0;
  return (
    <>
      {list && (
        <FilterImages>
          <FilterImage
            onPress={() => type != 1 && setType(1)}
            style={type == 1 && isactiveimg}
            activeOpacity={0.7}>
            <Image source={{uri: dinphoto}} />
            <Title style={type != 1 && {color: '#8D96A4'}}>Обед</Title>
          </FilterImage>
          <View style={{width: 8}} />
          <FilterImage
            onPress={() => type != 2 && setType(2)}
            style={type == 2 && isactiveimg}
            activeOpacity={0.7}>
            <Image source={{uri: brphoto}} />
            <Title style={type != 2 && {color: '#8D96A4'}}>Завтрак</Title>
          </FilterImage>
        </FilterImages>
      )}
      <FiltersRow style={list && {marginBottom: 8}}>
        <ScrollView
          horizontal
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={{paddingLeft: 8}}>
          <Touch onPress={toggle} activeOpacity={0.5}>
            <Filter style={(pricing || hastypes) && isactive}>Фильтр▼</Filter>
          </Touch>
          {!list && (
            <>
              <Touch
                onPress={() => type != 1 && setType(1)}
                activeOpacity={0.5}>
                <Filter style={type == 1 && isactive}>Обед</Filter>
              </Touch>
              <Touch
                onPress={() => type != 2 && setType(2)}
                activeOpacity={0.5}>
                <Filter style={type == 2 && isactive}>Завтрак</Filter>
              </Touch>
            </>
          )}
          <Touch onPress={toggle} activeOpacity={0.5}>
            <Filter style={pricing && isactive}>
              {!pricing ? 'Цена▼' : 'до ' + pricing + ' UAH▼'}
            </Filter>
          </Touch>
          <Touch onPress={toggle} activeOpacity={0.5}>
            <Filter style={hastypes && isactive}>
              {!hastypes
                ? 'Кухня▼'
                : types.size == 1
                ? [...types][0] + '▼'
                : types.size + ' кухни▼'}
            </Filter>
          </Touch>
        </ScrollView>
      </FiltersRow>
    </>
  );
};

const isactive = {backgroundColor: '#FFF5CA', borderColor: '#FFD93A'};
const isactiveimg = {backgroundColor: '#FFD93A'};

const Touch = styled.TouchableOpacity``;
const Press = styled.Pressable``;
const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;

const LogoRowView = styled(Row)`
  width: ${wwidth}px;
  justify-content: space-between;
  background-color: white;
  height: 56px;
  elevation: 5;
  padding-right: 8px;
`;

const SearchPress = styled(Press)`
  flex: 1;
`;

const InputRow = styled(Row)`
  flex: 1;
  border-radius: 20px;
  border: 1px solid #cfd4dc;
  margin: 10px 8px;
`;

const Text = styled.Text`
  font-family: 'Rubik-Regular';
  font-size: 12px;
  line-height: 14px;
`;

const SearchText = styled(Text)`
  color: #8d96a4;
  padding: 8px 12px;
`;

const Input = styled.TextInput`
  flex: 1;
  font-family: 'Rubik-Regular';
  font-size: 12px;
  line-height: 14px;
  color: black;
  padding: 8px 12px;
`;

const FilterImages = styled(Row)`
  background-color: white;
  height: 150px;
  padding: 12px 8px 8px;
`;

const FilterImage = styled(Touch)`
  justify-content: space-evenly;
  align-items: center;
  background: #f4f6f8;
  height: 130px;
  width: ${(wwidth - 8 * 3) / 2}px;
  border-radius: 8px;
`;

const Image = styled.Image`
  width: 69px;
  height: 70px;
`;

const Title = styled(Text)`
  color: black;
`;

const FiltersRow = styled(Row)`
  background-color: white;
  height: 45px;
  padding: 8px 0 7px;
`;

const Filter = styled(Text)`
  color: #222;
  border-radius: 8px;
  border: 1px solid #e5edff;
  height: 30px;
  padding: 7px 16px;
  margin-right: 8px;
`;

const Logo = props => (
  <Svg
    width={32}
    height={33}
    viewBox="0 0 32 33"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
    {...props}>
    <Path
      d="M11.852 0h2.963v32h-2.963V0zM17.038 0H20v32h-2.963V0z"
      fill="#000"
    />
    <Path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M3.482.001H0v27.185a4.815 4.815 0 109.63 0V.002H6.149v27.63a1.333 1.333 0 11-2.667 0V0zM27.11 0a4.889 4.889 0 00-4.888 4.889V27.11a4.889 4.889 0 009.778 0V4.89C32 2.189 29.81 0 27.11 0zm0 3.037c-.736 0-1.333.597-1.333 1.333v23.26a1.333 1.333 0 002.667 0V4.37c0-.736-.597-1.333-1.333-1.333z"
      fill="#000"
    />
  </Svg>
);
