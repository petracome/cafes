import React, {createContext, useContext, useState, useEffect} from 'react';
import fire from '@react-native-firebase/firestore';
const db = fire();
import useCContext from './CafeContext';

const MenuContext = createContext();
export default () => useContext(MenuContext);

export const MenuProvider = ({children}) => {
  const {cafe} = useCContext(),
    {id: cafeID, breaks: cbreaks, lunches: clunches} = cafe,
    menudb = db.doc('cafes/' + cafeID),
    [menu, setMenu] = useState(),
    {breaks, lunches} = menu || {},
    [brLength, lLength] = [breaks?.length || 0, lunches?.length || 0];

  useEffect(() => {
    (cbreaks || clunches) &&
      setMenu({breaks: cbreaks?.menu, lunches: clunches?.menu});
  }, [cbreaks, clunches]);

  const updateDBMenu = async (type, arr) => {
    await menudb
      .update({[`${type}.menu`]: arr})
      .catch(err =>
        alert(
          'Не получилось обновить, проверьте доступ в интернет и попробуйте еще раз.\n' +
            err.toString(),
        ),
      );
  };

  const updateLocalMenu = (type, arr) => setMenu(pr => ({...pr, [type]: arr}));

  const addPost = async (type, {time, desc, ...post}) => {
    let npost = {
      time,
      id: time.toString(),
      desc: desc?.trim()[0] ? desc.trim() : null,
      ...post,
    };
    try {
      await menudb
        .update({[`${type}.menu`]: fire.FieldValue.arrayUnion(npost)})
        .catch(er => console.error('addPost error', er));
      setMenu(pr => ({...pr, [type]: [...pr[type], npost]}));
    } catch (err) {
      alert(
        'Не получилось обновить меню, проверьте доступ в интернет и попробуйте еще раз.\n' +
          err.toString(),
      );
    }
  };

  const updatePost = async ({type, id, ind, ...post}) => {
    const tmenu = menu[type],
      i = !id ? ind : tmenu.findIndex(p => p.id == id),
      newTmenu = [
        ...tmenu.slice(0, i),
        {id: id || post.time?.toString() || null, ...post},
        ...tmenu.slice(i + 1),
      ];
    await updateDBMenu(type, newTmenu);
    updateLocalMenu(type, newTmenu);
  };

  const updatePostField = async ({type, id, ind, field, value}) => {
    const i = !id ? ind : menu[type].findIndex(p => p.id == id),
      newTMenu = [
        ...menu[type].slice(0, i),
        {...menu[type][i], [field]: value},
        ...menu[type].slice(i + 1),
      ];
    await updateDBMenu(type, newTMenu);
    setMenu(pr => ({
      ...pr,
      [type]: [
        ...pr[type].slice(0, i),
        {...pr[type][i], [field]: value},
        ...pr[type].slice(i + 1),
      ],
    }));
  };

  const delpost = async ({type, id, ind}) => {
    let newTmenu = [...menu[type]],
      i = !id ? ind : newTmenu.findIndex(p => p.id == id);
    newTmenu.splice(i, 1);
    await updateDBMenu(type, newTmenu);
    updateLocalMenu(type, newTmenu);
  };

  return (
    <MenuContext.Provider
      value={{
        cafeID,
        menu,
        brLength,
        lLength,
        updatePost,
        updatePostField,
        addPost,
        delpost,
      }}>
      {children}
    </MenuContext.Provider>
  );
};
