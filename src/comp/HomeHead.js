import React, {useState, useEffect, useCallback} from 'react';
import {FlatList, View} from 'react-native';
import styled from 'styled-components';
import fire from '@react-native-firebase/firestore';
const db = fire();
import usecontext from '../commons/AuthContext';
import useMContext from '../commons/MenuContext';

import {Tabs, Button, PageTitle, UsersIcon} from '../commons/IconsForms';

export default ({cafe, listref, navigate}) => {
  const {role} = usecontext(),
    isadmin = role == 'admin',
    {id, name, logo, kitchens, address} = cafe,
    types = ['breaks', 'lunches'].filter(it => cafe[it]),
    [tab, setTab] = useState(0);

  const tabpress = useCallback((index, noanim) => {
    setTab(index);
    listref?.current.scrollToIndex({index, animated: !noanim});
  }, []);

  // когда удалили активный type
  useEffect(() => {
    types[0] && !types[tab] && tabpress(0, 'noanim');
  }, [types]);

  //  когда добавили пост в неактивном type и вернулись на home, обновляем через сравнение числа постов
  const {brLength: cnxbrLength, lLength: cnxlLength} = useMContext(),
    [brlen, setBrlen] = useState(cnxbrLength),
    [llen, setLlen] = useState(cnxlLength),
    oldBrlen = cnxbrLength > brlen,
    oldLlen = cnxlLength > llen,
    lind = types.indexOf('lunches');
  useEffect(() => {
    ((!brlen && cnxbrLength) || oldBrlen) && setBrlen(cnxbrLength);
    ((!llen && cnxlLength) || oldLlen) && setLlen(cnxlLength);
    ((oldBrlen && tab != 0) || (oldLlen && tab != lind)) &&
      tabpress(oldBrlen ? 0 : lind, 'noanim');
  }, [oldBrlen, oldLlen]);

  const [applied, setApplied] = useState(),
    aplLength = applied?.length,
    hasApplies = aplLength > 0,
    dbstaff = db.doc('staff/' + id);

  useEffect(() => {
    const getstaff = async () =>
      await dbstaff.get().then(dc => setApplied(dc.data().applied));
    isadmin && id && getstaff();
  }, [role, id]);

  useEffect(() => {
    if (isadmin && applied) {
      return dbstaff.onSnapshot(dc => {
        let {applied: apls} = dc.data();
        apls.length != aplLength && setApplied(apls);
      });
    }
  }, [role, applied]);

  const renderTypes = ({item: it, index: i}) => {
    let {startTime, endTime, minCost, maxCost} = cafe[it],
      cost = minCost || cafe[it].cost,
      style = i == tab && {color: 'black'};
    return (
      <PriceRow>
        <FlexText {...{style}}>
          {it == 'breaks' ? 'Завтраки' : it == 'lunches' ? 'Обеды' : 'Ужины'}
        </FlexText>
        <FlexText {...{style}} selectable={true}>
          {startTime + '-' + endTime}
        </FlexText>
        <PriceComp
          active={i == tab}
          tx={
            ((!maxCost || maxCost <= cost) && 'от ') +
            cost +
            (maxCost && maxCost > cost ? '-' + maxCost : '') +
            ' UAH'
          }
        />
      </PriceRow>
    );
  };

  return (
    <>
      <PageTitleRow>
        <PageTitle>Меню</PageTitle>
        <Press onPress={() => navigate('Staff')}>
          <Row>
            {isadmin && hasApplies && <Marker {...{aplLength}} />}
            <UsersIcon />
          </Row>
        </Press>
      </PageTitleRow>

      <CafeCard>
        <Row>
          <Image source={{uri: logo}} />
          <TitleView>
            <CafeTitle numberOfLines={1}>{name}</CafeTitle>
            {kitchens && (
              <Tag numberOfLines={1}>
                {kitchens.map(k => k).join(', ') +
                  (kitchens.length > 1 ? ' кухни' : ' кухня')}
              </Tag>
            )}
            <Tag numberOfLines={1}>{address}</Tag>
          </TitleView>
        </Row>
        <FlatList
          data={types}
          keyExtractor={it => it}
          renderItem={renderTypes}
          {...{ItemSeparatorComponent}}
        />
        {isadmin && (
          <Button
            transp
            text="Редактировать"
            onPress={() => navigate('EditCafe')}
            style={{marginTop: 8}}
          />
        )}
      </CafeCard>

      <Tabs
        data={types.map(it => (it == 'breaks' ? 'Завтраки' : 'Обеды'))}
        {...{tab, setTab, listref, tabpress}}
      />
    </>
  );
};

const Press = styled.Pressable``;
const Row = styled.View`
  flex-direction: row;
`;

const Text = styled.Text`
  font-family: 'Rubik-Regular';
  font-size: 14px;
  line-height: 20px;
  flex-shrink: 1;
  color: black;
`;

const PageTitleRow = styled(Row)`
  justify-content: space-between;
  align-items: center;
  padding: 16px;
  padding-right: 18px;
`;

const Marker = ({aplLength: length}) => (
  <MarkerView>
    <MarkerText>
      {length + ' заяв' + (length == 1 ? 'ка' : length < 5 ? 'ки' : 'ок')}
    </MarkerText>
  </MarkerView>
);

const MarkerView = styled.View`
  background-color: #ff5e00;
  height: 22px;
  border-radius: 6px;
  padding: 5px 6px;
  margin: 2px -12px 0 0;
`;

const MarkerText = styled(Text)`
  font-family: 'Rubik-Medium';
  line-height: 14px;
  color: white;
`;

const CafeCard = styled.View`
  padding: 0 16px 16px;
`;

const TitleView = styled.View`
  flex: 1;
  flex-shrink: 1;
  padding: 3px 0 13px;
  /* background: red; */
`;

export const Image = styled.Image`
  width: 48px;
  height: 48px;
  border-radius: 25px;
  margin-right: 12px;
`;

const CafeTitle = styled(Text)`
  font-family: 'Rubik-Medium';
  font-size: 18px;
  line-height: 26px;
`;

const Tag = styled(Text)`
  color: #888;
`;

const FlexText = styled(Tag)`
  flex: 1;
`;

const PriceRow = styled(Row)`
  justify-content: space-between;
  padding: 5px 12px;
`;

const PriceComp = ({active, tx}) =>
  !active ? (
    <Price numberOfLines={1}>{tx}</Price>
  ) : (
    <ActivePrice numberOfLines={1}>{tx}</ActivePrice>
  );

const Price = styled(Tag)`
  width: 102px;
  text-align: right;
`;
const ActivePrice = styled(Price)`
  font-family: 'Rubik-Medium';
  color: #ff5e00;
`;

const ItemSeparatorComponent = styled.View`
  height: 1px;
  background: rgba(231, 231, 231, 0.4);
`;
