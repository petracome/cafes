import React, {useState} from 'react';
import {Linking} from 'react-native';
import styled from 'styled-components';
import {wwidth, breakf} from '../commons/models';

export default ({cafe}) => {
  const {
    id,
    name,
    logo,
    type,
    kitchens,
    address,
    timeStart,
    timeEnd,
    lcostMin,
    lcostMax,
    cost,
    location,
  } = cafe;
  return (
    <CardHead>
      <Logo source={{uri: logo}} />
      <TitleView>
        <Title selectable={true} numberOfLines={1}>
          {name}
        </Title>
        {(type || kitchens) && (
          <Tag style={{marginTop: 2}} selectable={true} numberOfLines={1}>
            {[type, ...(kitchens || [])].map(k => k).join(', ')}
          </Tag>
        )}
        <Row>
          <AdrText selectable={true} numberOfLines={1}>
            <Adress>{address}</Adress>
          </AdrText>
        </Row>
        <PriceRow>
          <Tag selectable={true}>{timeStart + ' - ' + timeEnd}</Tag>
          {(lcostMin || cost) && (
            <Price selectable={true}>
              {lcostMin || cost}
              {lcostMax && '-' + lcostMax} UAH
            </Price>
          )}
        </PriceRow>
      </TitleView>
    </CardHead>
  );
};

export const CafeBody = ({cafe, navigate}) => {
  const {menu, address, timeStart, timeEnd, phone, type2} = cafe;

  // const BContent = () =>  <Press onPress={() => navigate('Image', {url: breakf})}><MenuImage source={{uri: breakf}} /> </Press> );

  return (
    <>
      <Body>
        <InnerTags>
          {/* <Touch activeOpacity={0.5}>
              <TagTitle>Обед</TagTitle>
            </Touch>  
            <Press onPress={() => navigate('Image', {url: menu[menu.length - 1]})}>
              <MenuImage source={{uri: menu[menu.length - 1]}} />
            </Press> */}

          <TagTitle>О заведении</TagTitle>
        </InnerTags>

        <InfoView>
          <InfoGroup>
            <InfoTitle>Адрес</InfoTitle>
            <InfoText>{address}</InfoText>
          </InfoGroup>
          <InfoGroup>
            <InfoTitle>Контакты</InfoTitle>
            <Phone>{phone}</Phone>
          </InfoGroup>
          <InfoGroup>
            <InfoTitle>Время работы</InfoTitle>
            <InfoText>{timeStart + ' - ' + timeEnd}</InfoText>
          </InfoGroup>
        </InfoView>
      </Body>
    </>
  );
};

const width = wwidth - 8 * 2;

const Touch = styled.TouchableOpacity``;
const Press = styled.Pressable``;
const Row = styled.View`
  flex-direction: row;
`;

const CardHead = styled(Row)`
  flex-direction: row;
  background-color: white;
  flex-shrink: 1;
  padding: 16px 8px 0;
  height: 118px;
  border-radius: 8px;
`;

const Logo = styled.Image`
  width: 56px;
  height: 56px;
  border-radius: 10px;
  margin-right: 14px;
`;

const TitleView = styled.View`
  flex: 1;
`;

const Title = styled.Text`
  font-family: 'Rubik-Medium';
  font-size: 14px;
  line-height: 14px;
  color: black;
  flex-shrink: 1;
`;

const Tag = styled.Text`
  font-family: 'Rubik-Regular';
  font-size: 12px;
  line-height: 14px;
  color: #5b5e67;
  flex-shrink: 1;
`;

const AdrText = styled.Text`
  font-family: 'Rubik-Medium';
  font-size: 10px;
  color: #186cef;
  background-color: #e5edff;
  border-radius: 10px;
  height: 21px;
  padding: 3px 8px;
  margin-top: 8px;
`;
const Adress = styled(AdrText)`
  font-family: 'Rubik-Regular';
`;

const PriceRow = styled(Row)`
  justify-content: space-between;
  align-items: center;
  margin-top: 8px;
`;

const Price = styled(Title)`
  line-height: 17px;
  margin: 0;
`;

const Body = styled.View`
  flex: 1;
  margin: 8px 0 24px;
`;

const InnerTags = styled(Row)`
  flex: 1;
  background: white;
  border-top-width: 1px;
  border-top-color: #eaeaea;
  padding: 8px;
`;

const TagTitle = styled(Title)`
  font-size: 14px;
  line-height: 17px;
  border-bottom-color: #186cef;
  padding-bottom: 4px;
  margin-right: 16px;
`;

const MenuImage = styled.Image`
  width: ${width}px;
  height: ${width}px;
  border-radius: 8px;
  margin: 0 8px;
`;

const InfoView = styled.View`
  width: ${wwidth}px;
  margin: -16px 0;
`;

const InfoGroup = styled.View`
  border-bottom-width: 1px;
  border-bottom-color: #eaeaea;
  padding: 21px 10px 16px;
`;

const InfoTitle = styled(Title)`
  font-size: 12px;
`;

const InfoText = styled(InfoTitle)`
  font-family: 'Rubik-Regular';
  margin-top: 8px;
`;

const Phone = styled(InfoTitle)`
  color: #186cef;
  margin-top: 8px;
`;
