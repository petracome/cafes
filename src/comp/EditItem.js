import React, {useState, useEffect} from 'react';
import {Keyboard, ActivityIndicator} from 'react-native';
import styled from 'styled-components/native';
import {SendIcon} from '../commons/IconsForms';
import {
  handleColon,
  onInputEnd,
  checkTimeInp as checkTime,
  checkMCostInp as checkCost1,
} from '../commons/models';

export default ({i, tp: type, cafe, update, focused}) => {
  const [load, setLoad] = useState(false),
    i0 = i == 0,
    i1 = i == 1,
    i2 = i == 2,
    i3 = i == 3,
    istime = i0 || i1,
    field = i0 ? 'startTime' : i1 ? 'endTime' : i2 ? 'minCost' : 'maxCost',
    {[field]: fval, minCost: minc, cost, startTime} = cafe[type],
    minCost = minc || cost,
    initval = i2 ? minCost : fval,
    initstr = initval?.toString() || '',
    [value, setValue] = useState(initstr),
    changed = value.trim() != initstr;

  const onChangeText = tx =>
    setValue(pr => (!istime ? tx : handleColon(pr, tx)));

  const onEndEditing = () => setValue(onInputEnd(value, istime));

  const save = async () => {
    Keyboard.dismiss();
    if (!changed) return;
    if (istime && !checkTime(value))
      return alert('Время должно быть в формате ХХ:ХХ');
    if (i1 && value == startTime)
      return alert('Время с и Время до должны различаться');
    if (i2 && !checkCost1(parseInt(value)))
      return alert('Некорректная цена от');
    else {
      setLoad(true);
      await update(field, istime ? value : parseInt(value));
      setLoad(false);
    }
  };

  useEffect(() => {
    !focused && changed && setValue(initstr);
  }, [focused]);

  return (
    <FlexView>
      <Caption>
        {istime
          ? 'Время ' + (i0 ? 'c' : 'до')
          : 'Цена ' + (i2 ? 'от' : 'до') + ', UAH'}
      </Caption>
      <InputRow>
        <Input
          {...{value, onChangeText, onEndEditing}}
          onSubmitEditing={save}
          maxLength={istime ? 5 : i2 ? 3 : 4}
          returnKeyType="send"
          keyboardType={'number-pad'}
          style={
            ((i3 && value && parseInt(value) <= minCost) ||
              (i1 && value == startTime)) && {color: 'red'}
          }
        />
        {changed && !load && <SendIcon onPress={save} />}
        {load && <ActivityIndicator color="#ff5e00" style={{marginRight: 4}} />}
      </InputRow>
    </FlexView>
  );
};

export const FlexView = styled.View`
  flex: 1;
  padding-top: 8px;
  margin-right: 16px;
`;

export const Caption = styled.Text`
  font-family: 'Rubik-Regular';
  font-size: 12px;
  color: #888;
  flex-shrink: 1;
`;

export const InputRow = styled.View`
  flex-direction: row;
  align-items: center;
  border-bottom-width: 1px;
  border-bottom-color: #e7e7e7;
  padding-bottom: 2px;
`;

export const Input = styled.TextInput`
  font-family: 'Rubik-Regular';
  font-size: 16px;
  color: black;
  flex: 1;
  padding: 8px 0;
  /* background-color: red; */
`;
