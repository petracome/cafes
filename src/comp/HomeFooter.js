import React, {useState, useEffect, useCallback} from 'react';
import {FlatList, View} from 'react-native';
import styled from 'styled-components';
import {useIsFocused} from '@react-navigation/native';
import BottomSheet from '@gorhom/bottom-sheet';
import LinearGradient from 'react-native-linear-gradient';
import {wheight, paddBottom} from '../commons/models';
import {Button, PlusIcon, shadow16} from '../commons/IconsForms';

export default ({navigate, menuTypes}) => {
  const [opened, setOpened] = useState(false),
    isFocused = useIsFocused();

  const open = () => setOpened(true),
    close = () => setOpened(false),
    onAnimate = useCallback((from, to) => from != 0 && to == 0 && close(), []);

  useEffect(() => {
    !isFocused && opened && close();
  }, [isFocused]);

  const renderItem = ({item: type, index: i}) => (
    <Touch
      onPress={() =>
        navigate(menuTypes.includes(type) ? 'Edit' : 'NewMenu', {type})
      }
      activeOpacity={0.5}>
      <TypeText>{i == 0 ? 'Новый завтрак' : 'Новый обед'}</TypeText>
    </Touch>
  );

  const backdropComponent = () => <BackModal onPress={close} />;

  return (
    <>
      <LinearGradient
        colors={['rgba(255,255,255,0)', '#FFFEFE']}
        start={{x: 0, y: 0}}
        end={{x: 0, y: 1.0}}
        style={{
          position: 'absolute',
          bottom: 0,
          width: '100%',
          height: 64 + paddBottom,
        }}
      />
      {!opened && (
        <PlusTouch
          onPress={open}
          style={[shadow16, {bottom: paddBottom}]}
          activeOpacity={0.7}>
          <PlusIcon />
        </PlusTouch>
      )}
      {opened && (
        <BottomSheet
          index={1}
          snapPoints={[1, 170]}
          contentHeight={170}
          handleComponent={null}
          detached={true}
          bottomInset={24 + paddBottom}
          backgroundStyle={{backgroundColor: 'rgba(0,0,0,0)'}}
          {...{onAnimate, backdropComponent}}>
          <View style={{marginHorizontal: 16}}>
            <FlatList
              data={['breaks', 'lunches']}
              keyExtractor={it => it}
              {...{renderItem, ItemSeparatorComponent}}
              scrollEnabled={false}
              style={{
                borderRadius: 8,
                backgroundColor: 'white',
                marginBottom: 8,
              }}
            />
            <Button text="Отменить" onPress={close} big white />
          </View>
        </BottomSheet>
      )}
    </>
  );
};

const Press = styled.Pressable``;
const Touch = styled.TouchableOpacity``;

const PlusTouch = styled(Touch)`
  align-self: center;
  position: absolute;
`;

const TypeText = styled.Text`
  font-family: 'Rubik-Regular';
  font-size: 16px;
  line-height: 24px;
  padding: 16px;
`;

const BackModal = styled(Press)`
  position: absolute;
  bottom: 0px;
  background-color: rgba(0, 0, 0, 0.5);
  width: 100%;
  height: ${wheight}px;
`;

const ItemSeparatorComponent = styled.View`
  height: 1px;
  background: #e7e7e7;
`;
