import React from 'react';
import {Alert} from 'react-native';
import styled from 'styled-components/native';
import dayjs from 'dayjs';
import calendar from 'dayjs/plugin/calendar';
dayjs.extend(calendar);
import useAContext from '../commons/AuthContext';
import {Button as Butt, TrashIcon} from '../commons/IconsForms';
import {isIOS} from '../commons/models';

export default ({it: staff, ind, applied, update}) => {
  const {myid, role: myrole, time: mytime} = useAContext(),
    {name, email, role, time} = staff,
    admin = role == 'admin',
    isme = email == myid,
    editable = myrole == 'admin' && (!admin || mytime < time);

  const confirm = async (text, onPress) =>
    Alert.alert(
      'Подтвердите действие' + (!isme ? ' для ' + name : ''),
      text,
      [
        {text: 'Да', onPress},
        {text: 'Отмена', style: 'cancel'},
      ],
      {cancelable: true},
    );

  const Button = ({text, onPress, ask, transp}) => (
    <Butt
      {...{transp}}
      onPress={() => confirm(ask, onPress)}
      style={!transp && {flexShrink: 1, marginRight: 4}}>
      <ButtonText
        style={transp ? {color: 'black'} : {fontFamily: 'Rubik-Medium'}}>
        {text}
      </ButtonText>
    </Butt>
  );

  if (isme)
    return (
      <CardView>
        <Header isme {...staff} />
      </CardView>
    );

  if (applied)
    return (
      <CardView>
        <Header applied {...staff} />
        <Buttons>
          <Row>
            {['Официант', 'Администратор'].map((text, i) => {
              let onPress = async () =>
                await update(ind, 'approved', i == 1 && 'admin');
              return (
                <Button
                  ask={
                    'Назначить ' + (i == 0 ? 'официантом?' : 'администратором?')
                  }
                  {...{text, onPress}}
                  key={text}
                />
              );
            })}
            <Button
              transp
              text="Отклонить"
              ask="Отклонить заявку?"
              onPress={async () => await update(ind, 'denied')}
            />
          </Row>
        </Buttons>
      </CardView>
    );
  else
    return (
      <CardView>
        <Header {...staff} />
        {editable && (
          <Buttons style={{paddingTop: 10}}>
            <Button
              transp
              text={'Назначить ' + (admin ? 'официантом' : 'админом')}
              ask={
                'Изменить роль на ' + (admin ? 'Официант?' : 'Администратор?')
              }
              onPress={async () =>
                await update(ind, 'approved', !admin && 'admin')
              }
            />
            <IconPress
              onPress={() =>
                confirm(
                  'Закрыть доступ и удалить из сотрудников?',
                  async () => await update(ind, 'denied', null),
                )
              }>
              <TrashIcon size={20} />
            </IconPress>
          </Buttons>
        )}
      </CardView>
    );
};

const Header = ({photo, name, time, email, role, isme, applied}) => (
  <>
    <StatusRow>
      <UserPic {...{photo, name}} />
      {/* {isme && <Name style={{marginLeft: 10}}>Я</Name>} */}
      {!applied && (
        <Role>{role == 'admin' ? 'Администратор' : 'Официант'}</Role>
      )}
      {applied && (
        <Row>
          <Email>{timing(time)}</Email>
          <NewMark />
        </Row>
      )}
    </StatusRow>
    <NameView>
      <Name numberOfLines={1}>{!isme ? name : name}</Name>
      {applied && <Email numberOfLines={1}>{email}</Email>}
    </NameView>
  </>
);

const timing = time =>
  dayjs(time).calendar(null, {
    sameDay: 'в HH:mm сегодня',
    lastDay: 'вчера в HH:mm',
    lastWeek: 'D MMMM HH:mm',
    sameElse: 'D MMMM HH:mm',
  });

const Touch = styled.Pressable``;
const Press = styled.Pressable``;
const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;

const CardView = styled.View`
  border-bottom-width: 1px;
  border-bottom-color: #e7e7e7;
  padding: 14px 16px 12px;
`;

const StatusRow = styled(Row)`
  justify-content: space-between;
`;

const Image = styled.Image`
  width: 24px;
  height: 24px;
  border-radius: 14px;
`;

const NameView = styled.View`
  flex: 1;
  flex-shrink: 1;
  padding-top: 4px;
`;

const Text = styled.Text`
  font-family: 'Rubik-Regular';
  font-size: 14px;
  flex-shrink: 1;
`;

export const Name = styled(Text)`
  font-family: 'Rubik-Medium';
  font-size: 16px;
  line-height: 24px;
  /* background-color: red; */
`;

export const Email = styled(Text)`
  font-size: ${isIOS ? 13 : 12}px;
  line-height: 18px;
  color: #888;
  /* background-color: yellow; */
`;

const Role = styled(Email)`
  color: #ff5e00;
`;

const Buttons = styled(Row)`
  justify-content: space-between;
  padding: 10px 0 4px;
`;

const ButtonText = styled(Email)`
  color: white;
  text-align: center;
  flex-shrink: 1;
  padding: 4px 13px;
`;

const IconPress = styled(Press)`
  padding: 3px 16px;
  margin-right: -16px;
`;

const NewMark = () => (
    <NewView>
      <New>New</New>
    </NewView>
  ),
  NewView = styled.View`
    background-color: #ffc137;
    border-radius: 13px;
    padding: 4px 8px;
    margin: 0 3px 0 8px;
  `,
  New = styled(Text)`
    font-size: 12px;
    line-height: 14px;
  `;

const UserPic = ({photo, name}) =>
  photo ? (
    <Image source={{uri: photo}} />
  ) : (
    <BlankUser>
      <BlankUserChar>{name[0]}</BlankUserChar>
    </BlankUser>
  );

const BlankUser = styled.View`
  background-color: #59b7c9;
  width: 24px;
  height: 24px;
  border-radius: 18px;
  justify-content: center;
  align-items: center;
`;
const BlankUserChar = styled.Text`
  font-family: 'Rubik-SemiBold';
  font-size: 12px;
  color: #fff;
  text-transform: capitalize;
`;
