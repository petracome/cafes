import React, {useRef, useState, useEffect} from 'react';
import {View, Alert, Image} from 'react-native';
import styled from 'styled-components';
import FastImage from 'react-native-fast-image';
import dayjs from 'dayjs';
import calendar from 'dayjs/plugin/calendar';
dayjs.extend(calendar);
import {wwidth} from '../commons/models';
import {SettingsIcon, shadow16, Switcher} from '../commons/IconsForms';
const width = wwidth;

export default React.memo(
  ({it: post, type, ind, update, delpost, navigate}) => {
    const mounted = useRef(true),
      {id, photo, photoRatio: w2h, desc, active: cnActive, time} = post,
      [popup, setPopup] = useState(false),
      [height, setHeight] = useState(w2h ? width / w2h : 0),
      [active, setActive] = useState(cnActive);

    const closePopup = () => setPopup(false);

    const toggleActive = () => {
      setActive(pr => !pr);
      popup && closePopup();
      update({type, id, ind, field: 'active', value: !active});
    };

    const edit = () => (navigate('Edit', {type, id, ind}), closePopup());

    const remove = () =>
      Alert.alert(
        'Подтвердите удаление',
        'Удалить публикацию?',
        [
          {
            text: 'Да',
            onPress: () => (closePopup(), delpost({id, ind, type})),
          },
          {text: 'Отмена', style: 'cancel'},
        ],
        {cancelable: true},
      );

    useEffect(() => {
      photo &&
        !w2h &&
        Image.getSize(photo, (w, h) => {
          let rat = w / h;
          mounted.current && setHeight(width / rat),
            update({type, id, ind, field: 'photoRatio', value: rat});
        });
      return () => (mounted.current = false);
    }, [photo]);

    return (
      <Press onPress={closePopup}>
        <PostView>
          <View>
            {photo && (
              <Touch
                onPress={
                  popup ? closePopup : () => navigate('IMG', {url: photo})
                }
                activeOpacity={popup ? 1 : 0.8}>
                <ImageComp {...{height, photo}} />
              </Touch>
            )}
            <Body style={!photo && {minHeight: 130}}>
              {desc && <Desc>{desc}</Desc>}
              {time && (
                <Time>
                  {dayjs(time).calendar(null, {
                    sameDay: 'сегодня в HH:mm',
                    lastDay: 'вчера в HH:mm',
                    lastWeek: 'D MMMM в HH:mm',
                    sameElse: 'D MMMM в HH:mm',
                  })}
                </Time>
              )}
            </Body>
          </View>

          <StatusRow>
            <Switcher toggle={toggleActive} {...{active}} />
            <SettingsPress onPress={() => setPopup(!popup)}>
              <SettingsIcon />
            </SettingsPress>
          </StatusRow>

          {popup && (
            <SettingsView style={shadow16}>
              <Touch onPress={edit} activeOpacity={0.5}>
                <Setting>Редактировать</Setting>
              </Touch>
              <Touch onPress={remove} activeOpacity={0.5}>
                <Setting>Удалить</Setting>
              </Touch>
            </SettingsView>
          )}
        </PostView>
      </Press>
    );
  },
);

const Touch = styled.TouchableOpacity``;
const Press = styled.Pressable``;
const Row = styled.View`
  flex-direction: row;
  align-items: center;
`;

const PostView = styled.View`
  justify-content: space-between;
  padding: 16px 0 20px;
`;

const ImageComp = ({height, photo}) =>
  !height ? (
    <BlankImg />
  ) : (
    <BlankImg style={{height}}>
      <FastImage source={{uri: photo}} style={{width, height}} />
    </BlankImg>
  );

const BlankImg = ({style, children}) => (
  <BlankImgView {...{style}}>
    <BlankImgCircle />
    {children}
  </BlankImgView>
);

const BlankImgView = styled.View`
    background: #f5f5f5;
    width: ${width}px;
    height: ${width}px;
    justify-content: center;
    align-items: center;
    margin-bottom: 12px;
  `,
  BlankImgCircle = styled.View`
    position: absolute;
    /* left: ${width / 2 - 30}px; */
    width: 100px;
    height: 100px;
    border-radius: 50px;
    border: 3px solid white;
  `;

const Text = styled.Text`
  font-family: 'Rubik-Regular';
  font-size: 14px;
  flex-shrink: 1;
`;

const Body = styled.View`
  padding: 0 16px 12px;
`;

const Desc = styled(Text)`
  font-size: 16px;
  line-height: 24px;
`;

const StatusRow = styled(Row)`
  justify-content: space-between;
  padding: 0 16px;
  /* background: black; */
`;

const Time = styled(Text)`
  color: #b5b5b5;
  line-height: 24px;
`;

const SettingsPress = styled(Press)`
  padding: 10px;
  margin: -6px;
`;

const SettingsView = styled.View`
  position: absolute;
  bottom: 56px; //14px;
  right: 12px; //52px;
  /* z-index: 2; */
  background: #fffefe;
  border-radius: 8px;
`;

const Setting = styled(Text)`
  font-size: 16px;
  line-height: 24px;
  padding: 12px 16px;
`;
